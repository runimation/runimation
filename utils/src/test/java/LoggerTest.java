import at.runimation.utils.OSHelper;
import at.runimation.utils.RunimationLogger;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.io.IoBuilder;

class LoggerTest {
    public static void main(String[] args) {
        System.setOut(IoBuilder.forLogger(LogManager.getLogger("STDOUT"))
                .setLevel(Level.INFO)
                .buildPrintStream()
        );

        System.setErr(IoBuilder.forLogger(LogManager.getLogger("STDERR"))
                .setLevel(Level.ERROR)
                .buildPrintStream()
        );

        RunimationLogger.getLogger("test").trace("trace");
        RunimationLogger.getLogger("test").debug("debug");
        RunimationLogger.getLogger("test").info("info");
        RunimationLogger.getLogger("test").warn("warn");
        RunimationLogger.getLogger("test").error("error");
        RunimationLogger.getLogger("test").fatal("fatal");

        RunimationLogger.getLogger("Compiler").warn("This is a warning");

        System.out.println("This is text on stdout!");
        System.out.println(System.getProperty("os.name"));

        System.out.println("Is this windows? " + (OSHelper.isWindows() ? "Yes" : "No"));
        System.out.println("Is this unix? " + (OSHelper.isUnix() ? "Yes" : "No"));
    }
}
