package at.runimation.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class RunimationLogger {
    public static Logger getLogger(String name) {
        return LogManager.getLogger(name);
    }

    public static Logger getCompiliationLogger() {
        return LogManager.getLogger("compile");
    }
}
