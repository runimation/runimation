/*
 * Copyright 2018 Stefan Grünzinger, Florian Schachermair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.runimation.compiler;

import at.runimation.compiler.exception.CompilerException;
import at.runimation.log.LogOut;
import at.runimation.utils.IOHelper;
import at.runimation.utils.OSHelper;
import org.joor.Reflect;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;
import java.io.*;
import java.nio.file.Files;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class CompileUnit {
    private static final String PROGRAM_CLASS = "at.runimation.compiler.run.Main";

    private final File sourceFile;
    private final File zipFile;
    private final File buildDirectory;
    private final PrintStream logStream;

    private final StringBuilder sourceCodeBuilder = new StringBuilder();

    private Runnable programRunnable;

    public CompileUnit(File sourceFile, File zipFile, String name, PrintStream logStream) {
        this(sourceFile, zipFile, new File(new File(System.getProperty("java.io.tmpdir"), "runimation"), name), logStream);
    }

    public CompileUnit(File sourceFile, File zipFile, File buildDirectory, PrintStream logStream) {
        this.sourceFile = sourceFile;
        this.zipFile = zipFile;
        this.buildDirectory = buildDirectory;
        this.logStream = logStream;
    }

    public boolean compileProgram() throws CompilerException {
        if(sourceCodeBuilder.toString().isEmpty())
            throw new CompilerException("Instrumentation not compiled");

        programRunnable = Reflect.compile(PROGRAM_CLASS, sourceCodeBuilder.toString()).create().get();

        return true;
    }

    public boolean collectData() throws CompilerException {
        if(programRunnable == null)
            throw new CompilerException("Program not compiled");

        if(!buildDirectory.mkdirs()) {
            throw new CompilerException("Could not create build directory");
        }

        LogOut.reset();
        try {
            programRunnable.run();
        } catch (RuntimeException e) {
            throw new CompilerException("Exception raised while trying to collect Data: ", e);
        }
        LogOut.writeLog(new File(buildDirectory, "log.json"));

        return true;
    }

    public boolean compileInstrumentation() {
        try {
            String sourceCode = IOHelper.readIntoString(sourceFile);
            String parsed = SourceParser.parseSourceCode(sourceCode, logStream);

            try (BufferedReader reader = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/Main.frame")))) {
                String line;
                while ((line = reader.readLine()) != null) {
                    sourceCodeBuilder.append(line.replace("--> CODE", parsed)).append("\n");
                }

                System.out.println(sourceCodeBuilder.toString());

                return true;
            } catch (IOException e) {
                e.printStackTrace(logStream);
            }
        } catch (IOException | CompilerException e) {
            e.printStackTrace();
        }

        return false;
    }

    public void packageFiles() {
        new File(zipFile.getParent()).mkdirs();

        try (ZipOutputStream outputStream = new ZipOutputStream(new FileOutputStream(zipFile))) {

            ZipEntry logEntry = new ZipEntry("log.json");
            outputStream.putNextEntry(logEntry);
            copyFileToStream(new File(buildDirectory, "log.json"), outputStream);
            outputStream.closeEntry();

            ZipEntry sourceEntry = new ZipEntry("Main.java");
            outputStream.putNextEntry(sourceEntry);
            copyFileToStream(sourceFile, outputStream);
            outputStream.closeEntry();

            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace(logStream);
        }

    }

    private void copyFileToStream(File inputFile, OutputStream outputStream) {

        try (InputStream is = new FileInputStream(inputFile)) {

            byte[] data = new byte[1024];
            int bytesRead;

            while ((bytesRead = is.read(data)) != -1) {
                outputStream.write(data, 0, bytesRead);
            }

            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace(logStream);
        }
    }
}
