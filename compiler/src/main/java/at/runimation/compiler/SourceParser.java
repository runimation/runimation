/*
 * Copyright 2018 Stefan Grünzinger, Florian Schachermair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.runimation.compiler;

import at.runimation.compiler.exception.CompilerException;

import java.io.ByteArrayInputStream;
import java.io.PrintStream;
import java.nio.charset.Charset;

public class SourceParser {
    private static String parseResult = "";
    private static boolean isCompare = false;
    private static String compareString = "";

    public static String parseSourceCode(String sourceCode, PrintStream logStream) throws CompilerException {
        int lineNum = 0;
        StringBuilder newSource = new StringBuilder();

        String[] lines = sourceCode.split("\n");

        for(String line : lines) {
            lineNum++;
            int instIdx = line.indexOf("//$");

            if (instIdx == -1) {
                newSource.append(line).append("\n");
                continue;
            }

            String code = line.substring(0, instIdx);

            String[] instStrings = line.substring(instIdx + 3).trim().split(";");

            StringBuilder visBuilder = new StringBuilder();

            for (String instString : instStrings) {
                Scanner scanner = new Scanner(new ByteArrayInputStream(instString.getBytes(Charset.forName("UTF-8"))));
                Parser parser = new Parser(scanner, logStream);

                parser.Parse();

                if (parser.errors.count > 0) {
                    throw new CompilerException(parser.errors.count + " errors while parsing line " + lineNum);
                }

                parseResult = parseResult.replace("__LINE__", String.valueOf(lineNum));

                if(isCompare) {
                    if(!code.contains(compareString))
                        logStream.println("Compare string not found in line " + lineNum + ": " + compareString);
                    else
                        code = code.replace(compareString, parseResult);
                }
                else {
                    visBuilder.append(parseResult);
                }
            }

            newSource.append(code);
            newSource.append(visBuilder);

            newSource.append("\n");
        }
        return newSource.toString();
    }

    public static void setParseResult(String result, boolean isCompare, String compareString) {
        parseResult = result;
        SourceParser.isCompare = isCompare;
        SourceParser.compareString = compareString;
    }

    public static String getParseResult() {
        return parseResult;
    }
}
