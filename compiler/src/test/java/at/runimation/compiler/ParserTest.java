/*
 * Copyright 2018 Stefan Grünzinger, Florian Schachermair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.runimation.compiler;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.nio.charset.Charset;

import static org.junit.Assert.assertEquals;

public class ParserTest {
    @Test
    public void parse() {
        assertEquals(parseLine("i = 0"), 0);
        assertEquals(parseLine("a[5] = b"), 0);
        assertEquals(parseLine("a[x] = i + 1"), 0);
        assertEquals(parseLine("a[a.length - 1] = 7"), 0);
        assertEquals(parseLine("a[a.length - 1] = b[a.length - 1 - i]"), 0);

        parseLine("inc x");
        parseLine("comp a[i++] < x");
    }

    int parseLine(String line) {
        Scanner scanner = new Scanner(new ByteArrayInputStream(line.getBytes(Charset.forName("UTF-8"))));
        Parser parser = new Parser(scanner, System.err);

        parser.Parse();

        System.out.println(SourceParser.getParseResult());

        return parser.errors.count;
    }
}