# Runimation

Runimation is a program that should make getting into programming easier for beginners, but it should also help a bit more advanced programmers to understand a broad selection of Algorithms. Because pure theory often cannot stress the relevance and flexibility of certain algorithms enough, a practical part is often included into the lessons. The big problem is, that as soon as the algorithms get more complicated it is basically impossible to explain them step by step. Even though most modern development environments include debuggers, these are most of the time not beginner friendly. So the user has no other choice than just running the program and hoping all goes well, if it does not, the big search begins. Although by looking for errors students can learn to better find errors, but this does not teach them anything about the algorithm that should be taught.

To overcome all these problems, the program Runimation was created. It offers an uncluttered user interface in a modern and minimalistic style, without sacrificing any features needed to visualise some more complex algorithms. To ease the use of a projector, the visualiser part and the control part of the program can be separated. Furthermore the teacher can move freely around and still control the program, because presenters are supported by the program. To get the visualiser to show anything, the algorithm needs to be instrumented. This is done by a strong instrumentation language, that supports the most common operations. 

## Project Structure

The project is divided in the following subprojects

- **utils**: Various utilies used in the other projects
- **compiler**: The runimation compiler and parser
- **log**: The subproject responsible for the data collection and storage
- **vis**: The visualisation part of the project

## Build and run the project

The project uses gradle as a build system, but a gradle installation is not needed, since gradle will self install. 

 1. Install Java (version 8 or higher) and JavaFX 
    - Windows and Mac: Download the JDK from [here](http://www.oracle.com/technetwork/java/javase/downloads/index.html) 
    - Ubuntu/Debian: `sudo apt install default-jdk openjfx`
 3. Clone git repository: 
    - `git clone https://bitbucket.org/runimation/runimation` 
 4. Go into root folder: 
    - `cd runimation`
 5. Build the project
    - Windows: `gradlew build`
    - Linux and Mac: `./gradlew build`
 6. Run the project
    - Windows: `gradlew vis:run`
    - Linux and Mac: `./gradlew vis:run`
    
To create a standalone jar instead, you have to run the following command:
   
   - Windows `gradlew vis:shadowJar`
   - Linux and Mac: `./gradlew vis:shadowJar`

If the build was successful, the jar can be found at `./vis/build/libs` with a filename like `vis-1.0-all.jar`.