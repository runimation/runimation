/*
 * Copyright 2018 Stefan Grünzinger, Florian Schachermair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.runimation.vis;

import at.runimation.log.Vis;
import at.runimation.vis.gui.mainWindow.MainWindowController;
import at.runimation.vis.gui.mainWindow.MainWindowModel;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

import java.io.File;
import java.util.ResourceBundle;

public class Main extends Application {
    public static File programFile;

    public static void main(String[] args) {
        programFile = new File(Vis.class.getProtectionDomain().getCodeSource().getLocation().getPath());
        System.out.println(programFile.toString());
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/main_window/main_window.fxml"));
        loader.setResources(ResourceBundle.getBundle("bundles.strings"));
        Parent root = loader.load();

        MainWindowController controller = loader.getController();
        controller.setupController(primaryStage);

        Scene scene = new Scene(root, 1280, 720);

        scene.getStylesheets().add(JavaHighlighter.STYLESHEET_PATH);

        scene.addEventFilter(KeyEvent.KEY_PRESSED, controller::presenterInput);

        primaryStage.setOnCloseRequest(event -> Platform.exit());

        primaryStage.getIcons().add(new Image(Main.class.getResourceAsStream("/images/logo.png")));

        primaryStage.titleProperty().bind(MainWindowModel.getInstance().windowTitleProperty());
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
