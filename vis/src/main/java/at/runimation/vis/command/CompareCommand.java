/*
 * Copyright 2018 Stefan Grünzinger, Florian Schachermair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.runimation.vis.command;

import at.runimation.log.action.CompareAction;
import at.runimation.log.action.IAction;
import at.runimation.log.designator.ConstDesignator;
import at.runimation.log.designator.ElemDesignator;
import at.runimation.log.designator.VarDesignator;
import at.runimation.vis.gui.GUIConfig;
import at.runimation.vis.Model;
import at.runimation.vis.ModelProcessor;
import at.runimation.vis.animation.AnimationController;
import at.runimation.vis.animation.CompareAnimation;
import at.runimation.vis.transaction.ITransaction;
import at.runimation.vis.view.NumberView;

public class CompareCommand extends Command<CompareAction> {
    public CompareCommand(IAction action) {
        super(action);
    }

    @Override
    public void execute(AnimationController controller) {
        CompareAnimation.Builder animationBuilder = new CompareAnimation.Builder();
        animationBuilder.setCompareType(getAction().getCompareType());
        animationBuilder.setResult(getAction().getResult());

        // *** This is a comparison, so no values need to be changed! ***

        ITransaction rightTrans = getAction().getRightDesignator().process(ModelProcessor.INSTANCE);

        NumberView leftView = getAction().getLeftDesignator().process(ViewProcessor.INSTANCE);
        animationBuilder.setFirst(leftView);

        NumberView rightView = getAction().getRightDesignator().process(ViewProcessor.INSTANCE);
        if(rightView == null)
            animationBuilder.setSecond(rightTrans.getValue());
        else
            animationBuilder.setSecond(rightView);

        if (GUIConfig.getInstance().doCompareAnimationProperty().get()) {
            controller.startAnimation(animationBuilder.build());
        } else {
            controller.startWithoutAnimation(animationBuilder.build());
        }
    }
}
