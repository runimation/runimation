/*
 * Copyright 2018 Stefan Grünzinger, Florian Schachermair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.runimation.vis.command;

import at.runimation.log.action.IAction;
import at.runimation.log.action.InitArrAction;
import at.runimation.vis.gui.GUIConfig;
import at.runimation.vis.Model;
import at.runimation.vis.animation.Animation;
import at.runimation.vis.animation.AnimationController;
import at.runimation.vis.animation.InitArrayAnimation;
import at.runimation.vis.view.ArrayView;

public class InitArrCommand extends Command<InitArrAction> {
    InitArrCommand(IAction action) {
        super(action);
    }

    @Override
    public void execute(AnimationController controller) {
        // TODO: implement this weird edge case
        Model.getInstance().setVarValue(getAction().getName(), getAction().getElements());

        ArrayView arrayView = Model.getInstance().getView(getAction().getName());
        Animation animation = new InitArrayAnimation(arrayView, getAction().getElements());

        if (GUIConfig.getInstance().doInitArrAnimationProperty().get()) {
            controller.startAnimation(animation);
        } else {
            controller.startWithoutAnimation(animation);
        }
    }
}
