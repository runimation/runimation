/*
 * Copyright 2018 Stefan Grünzinger, Florian Schachermair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.runimation.vis.command;

import at.runimation.log.action.AnimateAction;
import at.runimation.log.action.IAction;
import at.runimation.vis.animation.AnimationController;

public class AnimateCommand extends Command<AnimateAction> {
    public AnimateCommand(IAction action) {
        super(action);
    }

    @Override
    public void execute(AnimationController controller) {

    }
}
