package at.runimation.vis.command;

import at.runimation.log.designator.ConstDesignator;
import at.runimation.log.designator.ElemDesignator;
import at.runimation.log.designator.IDesignatorProcessor;
import at.runimation.log.designator.VarDesignator;
import at.runimation.vis.Model;
import at.runimation.vis.view.NumberView;

import java.util.Map;

public class ViewProcessor implements IDesignatorProcessor<NumberView> {
    public static final ViewProcessor INSTANCE = new ViewProcessor();

    private ViewProcessor() {}

    @Override
    public NumberView process(ConstDesignator des, Map<String, Object> params) {
        // Return null to signify, that no view is available
        return null;
    }

    @Override
    public NumberView process(VarDesignator des, Map<String, Object> params) {
        return Model.getInstance().getNumberView(des.getName());
    }

    @Override
    public NumberView process(ElemDesignator des, Map<String, Object> params) {
        return Model.getInstance().getNumberView(des.getArray(), des.getIndex());
    }
}
