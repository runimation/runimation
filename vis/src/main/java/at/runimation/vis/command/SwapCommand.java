/*
 * Copyright 2018 Stefan Grünzinger, Florian Schachermair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.runimation.vis.command;

import at.runimation.log.action.IAction;
import at.runimation.log.action.SwapAction;
import at.runimation.log.designator.ElemDesignator;
import at.runimation.log.designator.VarDesignator;
import at.runimation.vis.gui.GUIConfig;
import at.runimation.vis.Model;
import at.runimation.vis.ModelProcessor;
import at.runimation.vis.animation.Animation;
import at.runimation.vis.animation.AnimationController;
import at.runimation.vis.animation.SwapAnimation;
import at.runimation.vis.transaction.ITransaction;
import at.runimation.vis.view.NumberView;

import java.util.Collections;

public class SwapCommand extends Command<SwapAction> {
    public SwapCommand(IAction action) {
        super(action);
    }

    @Override
    public void execute(AnimationController controller) {
        ITransaction leftTrans = getAction().getLeftDesignator().process(ModelProcessor.INSTANCE);
        ITransaction rightTrans = getAction().getRightDesignator().process(ModelProcessor.INSTANCE);

        leftTrans.swapWith(rightTrans);

        NumberView leftView = getAction().getLeftDesignator().process(ViewProcessor.INSTANCE);
        NumberView rightView = getAction().getRightDesignator().process(ViewProcessor.INSTANCE);

        if(leftView != null && rightView != null) {
            Animation animation = new SwapAnimation(leftView, rightView);

            if (GUIConfig.getInstance().doSwapAnimationProperty().get()) {
                controller.startAnimation(animation);
            } else {
                controller.startWithoutAnimation(animation);
            }
        }
    }
}
