/*
 * Copyright 2018 Stefan Grünzinger, Florian Schachermair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.runimation.vis.gui;

import javafx.beans.property.*;
import javafx.scene.text.Font;
import javafx.util.Duration;

public final class GUIConfig {
    private static GUIConfig instance;
    private DoubleProperty animationDuration;

    private ObjectProperty<Font> codeFont;
    private ObjectProperty<Font> animationFont;

    private BooleanProperty doAssignAnimation;
    private BooleanProperty doCompareAnimation;
    private BooleanProperty doIncrementAnimation;
    private BooleanProperty doDecrementAnimation;
    private BooleanProperty doInitArrAnimation;
    private BooleanProperty doSwapAnimation;

    private GUIConfig() {
    }

    public static GUIConfig getInstance() {
        if (instance == null) {
            instance = new GUIConfig();
        }
        return instance;
    }

    public DoubleProperty animationDurationProperty() {
        if (animationDuration == null) {
            animationDuration = new SimpleDoubleProperty();
        }

        return animationDuration;
    }

    public Duration getDuration() {
        double durationProperty = animationDurationProperty().getValue();
        return Duration.millis(1000 / durationProperty);
    }

    public ObjectProperty<Font> codeFontProperty() {
        if (codeFont == null) {
            codeFont = new SimpleObjectProperty<>();
        }

        return codeFont;
    }

    public ObjectProperty<Font> animationFontProperty() {
        if (animationFont == null) {
            animationFont = new SimpleObjectProperty<>();
        }

        return animationFont;
    }

    public BooleanProperty doAssignAnimationProperty() {
        if (doAssignAnimation == null) {
            doAssignAnimation = new SimpleBooleanProperty();
        }

        return doAssignAnimation;
    }

    public BooleanProperty doCompareAnimationProperty() {
        if (doCompareAnimation == null) {
            doCompareAnimation = new SimpleBooleanProperty();
        }

        return doCompareAnimation;
    }

    public BooleanProperty doIncrementAnimationProperty() {
        if (doIncrementAnimation == null) {
            doIncrementAnimation = new SimpleBooleanProperty();
        }

        return doIncrementAnimation;
    }

    public BooleanProperty doDecrementAnimationProperty() {
        if (doDecrementAnimation == null) {
            doDecrementAnimation = new SimpleBooleanProperty();
        }

        return doDecrementAnimation;
    }

    public BooleanProperty doInitArrAnimationProperty() {
        if (doInitArrAnimation == null) {
            doInitArrAnimation = new SimpleBooleanProperty();
        }

        return doInitArrAnimation;
    }

    public BooleanProperty doSwapAnimationProperty() {
        if (doSwapAnimation == null) {
            doSwapAnimation = new SimpleBooleanProperty();
        }

        return doSwapAnimation;
    }
}
