package at.runimation.vis.gui;

import javafx.stage.Stage;

public interface IWindowController {
    void setStage(Stage stage);
}
