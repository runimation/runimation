package at.runimation.vis.gui.projectorWindow;

import at.runimation.vis.JavaHighlighter;
import at.runimation.vis.compileWindow.CompileWindowController;
import at.runimation.vis.controls.AnimationPane;
import at.runimation.vis.controls.RunimationCodeArea;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProjectorWindowController {
    public AnchorPane animationPaneHolder;
    public SplitPane splitPane;

    private Stage windowStage;

    private RunimationCodeArea codeArea;
    private AnimationPane animationPane;

    public static Optional<Stage> createNewWindow(Screen screen, RunimationCodeArea codeArea, AnimationPane animationPane, BiConsumer<RunimationCodeArea, TitledPane> originalReplacer) {
        FXMLLoader loader = new FXMLLoader(CompileWindowController.class.getResource("/projector_window/projector_window.fxml"));
        Parent root;

        try {
            root = loader.load();
        } catch (IOException e) {
            Logger logger = Logger.getLogger(CompileWindowController.class.getName());
            logger.log(Level.SEVERE, "Failed to create projector window!", e);

            return Optional.empty();
        }

        Stage stage = new Stage();

        ProjectorWindowController controller = loader.getController();

        controller.setStage(stage);
        controller.setCodeArea(codeArea);
        controller.setAnimationPane(animationPane);

        stage.setScene(new Scene(root));

        stage.getScene().getStylesheets().add(JavaHighlighter.STYLESHEET_PATH);

        // Create a borderless window over the whole screen
        stage.setX(screen.getBounds().getMinX());
        stage.setY(screen.getBounds().getMinY());

        stage.setWidth(screen.getBounds().getWidth());
        stage.setHeight(screen.getBounds().getHeight());

        stage.initStyle(StageStyle.UNDECORATED);

        stage.setOnCloseRequest(event -> originalReplacer.accept(codeArea, animationPane));

        return Optional.of(stage);
    }

    private void setStage(Stage stage) {
        this.windowStage = stage;
    }

    private void setCodeArea(RunimationCodeArea codeArea) {
        this.codeArea = codeArea;

        splitPane.getItems().add(0, codeArea);
    }

    private void setAnimationPane(AnimationPane animationPane) {
        this.animationPane = animationPane;

        animationPaneHolder.getChildren().add(animationPane);
    }
}
