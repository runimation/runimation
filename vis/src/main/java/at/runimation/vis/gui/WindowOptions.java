package at.runimation.vis.gui;

import javafx.scene.image.Image;
import javafx.stage.Window;

import java.util.function.Supplier;

public class WindowOptions {
    private String title;
    private Window owner;
    private Image windowIcon;
    private Supplier<IWindowController> controllerFactory;

    public boolean hasTitle() {
        return getTitle() != null && !getTitle().isEmpty();
    }

    public String getTitle() {
        return title;
    }

    public WindowOptions setTitle(String title) {
        this.title = title;
        return this;
    }

    public boolean hasOwner() {
        return getOwner() != null;
    }

    public Window getOwner() {
        return owner;
    }

    public WindowOptions setOwner(Window owner) {
        this.owner = owner;
        return this;
    }

    public boolean hasWindowIcon() {
        return getWindowIcon() != null;
    }

    public Image getWindowIcon() {
        return windowIcon;
    }

    public WindowOptions setWindowIcon(Image windowIcon) {
        this.windowIcon = windowIcon;
        return this;
    }

    public boolean hasControllerFactory() {
        return getControllerFactory() != null;
    }

    public Supplier<IWindowController> getControllerFactory() {
        return controllerFactory;
    }

    public WindowOptions setControllerFactory(Supplier<IWindowController> controllerFactory) {
        this.controllerFactory = controllerFactory;
        return this;
    }
}
