package at.runimation.vis.gui;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BaseWindow extends Stage {
    public BaseWindow(URL windowResource, WindowOptions options) {
        try {
            Callback<Class<?>, Object> controllerFactory = null;
            if(options.hasControllerFactory()) {
                controllerFactory = param -> {
                    IWindowController controller = options.getControllerFactory().get();
                    controller.setStage(this);
                    return controller;
                };
            }

            FXMLLoader loader = new FXMLLoader(windowResource,
                    null,
                    null,
                    controllerFactory);
            Parent root = loader.load();
            setScene(new Scene(root));

            if(options.hasTitle())
                setTitle(options.getTitle());
            if(options.hasOwner())
                initOwner(options.getOwner());
            if(options.hasWindowIcon())
                getIcons().add(options.getWindowIcon());
        } catch (IOException e) {
            Logger logger = Logger.getLogger(getClass().getName());
            logger.log(Level.SEVERE, "Failed to create compiler window!", e);
        }
    }
}
