/*
 * Copyright 2018 Stefan Grünzinger, Florian Schachermair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.runimation.vis.gui.mainWindow;

import at.runimation.log.action.IAction;
import at.runimation.utils.OSHelper;
import at.runimation.vis.gui.GUIConfig;
import at.runimation.vis.Model;
import at.runimation.vis.animation.AnimationController;
import at.runimation.vis.command.Command;
import at.runimation.vis.compileWindow.CompileWindowController;
import at.runimation.vis.controls.AnimationPane;
import at.runimation.vis.controls.RunimationCodeArea;
import at.runimation.vis.fontSizeDialog.FontSizeWindow;
import at.runimation.vis.gui.projectorWindow.ProjectorWindowController;
import at.runimation.vis.view.FileDialogBuilder;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import javafx.util.Duration;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public final class MainWindowController {
    public AnchorPane animationAnchorPane;
    public Button playButton;
    public Button stepButton;
    public Slider speedSlider;
    public Label speedField;
    public Button resetButton;
    public CheckBox swapCheckBox;
    public CheckBox assignCheckBox;
    public CheckBox compareCheckBox;
    public CheckBox decrementCheckBox;
    public CheckBox incrementCheckBox;
    public CheckBox initArrCheckBox;
    public MenuItem projectorPopout;
    public SplitPane splitPane;
    public AnimationPane animationPane;

    private Stage windowStage;

    private Timeline actionTimeline;
    private boolean isPlaying;

    private Image imagePlay;
    private Image imageStop;

    private RunimationCodeArea codeArea;

    private AnimationController animationController;

    private boolean projectorWindowActive = false;
    private Stage projectorWindow;

    private ResourceBundle stringResources;

    @FXML
    void initialize() {
        stringResources = ResourceBundle.getBundle("bundles.strings");

        MainWindowModel.getInstance().windowTitleProperty().setValue("Runimation");

        setButtonIcons();

        codeArea = new RunimationCodeArea();
        splitPane.getItems().add(0, codeArea);
        resetCodeArea();

        initAnimationTimeline();
        initSpeedSlider();
        initAnimation();

        if(OSHelper.isWindows()) {
            GUIConfig.getInstance().codeFontProperty().setValue(Font.font("Monospaced", 18));
            GUIConfig.getInstance().animationFontProperty().setValue(Font.font("System", 20));
        } else {
            GUIConfig.getInstance().codeFontProperty().setValue(Font.font("Monospaced", 14));
            GUIConfig.getInstance().animationFontProperty().setValue(Font.font("System", 16));
        }

        animationController = new AnimationController(animationPane);
    }

    public void setupController(Stage stage) {
        this.windowStage = stage;
    }

    private void resetCodeArea() {
        codeArea.setSourceCode(Model.getInstance().getSourceCode());
    }

    private void initSpeedSlider() {
        speedSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.doubleValue() <= 0.1) {
                newValue = 0.1;
            }
            BigDecimal value = new BigDecimal(newValue.doubleValue());
            value = value.setScale(2, RoundingMode.FLOOR);

            speedField.setText(value.doubleValue() + " Steps / sec");

            boolean wasPlaying = actionTimeline.getStatus() == Animation.Status.RUNNING;


            if (wasPlaying) {
                actionTimeline.stop();
            }

            actionTimeline.getKeyFrames().clear();
            actionTimeline.getKeyFrames().add(
                    new KeyFrame(
                            Duration.millis(
                                    new BigDecimal(1000.0 / value.doubleValue())
                                            .setScale(0, RoundingMode.FLOOR)
                                            .doubleValue()
                            ),
                            event -> {event.consume(); executeNext();}
                    )
            );

            if (wasPlaying) {
                actionTimeline.play();
            }

        });

        speedSlider.setMin(0.0);
        speedSlider.setMax(5.0);
        speedSlider.setValue(1.0);
        speedSlider.setBlockIncrement(0.25);
        speedSlider.setMajorTickUnit(0.25);
        speedSlider.setMinorTickCount(0);
        speedSlider.setSnapToTicks(true);
    }

    private void initAnimationTimeline() {
        actionTimeline = new Timeline(new KeyFrame(Duration.millis(1000.0), event -> executeNext()));
        actionTimeline.setCycleCount(Animation.INDEFINITE);
        isPlaying = false;
    }

    private void initAnimation() {
        GUIConfig.getInstance().animationDurationProperty().bind(speedSlider.valueProperty().add(0.1));

        GUIConfig.getInstance().doAssignAnimationProperty().bindBidirectional(assignCheckBox.selectedProperty());
        GUIConfig.getInstance().doCompareAnimationProperty().bindBidirectional(compareCheckBox.selectedProperty());
        GUIConfig.getInstance().doDecrementAnimationProperty().bindBidirectional(decrementCheckBox.selectedProperty());
        GUIConfig.getInstance().doIncrementAnimationProperty().bindBidirectional(incrementCheckBox.selectedProperty());
        GUIConfig.getInstance().doInitArrAnimationProperty().bindBidirectional(initArrCheckBox.selectedProperty());
        GUIConfig.getInstance().doSwapAnimationProperty().bindBidirectional(swapCheckBox.selectedProperty());
    }

    private void startAutoPlay() {
        isPlaying = true;
        actionTimeline.play();
        playButton.setGraphic(new ImageView(imageStop));
    }

    private void stopAutoPlay() {
        isPlaying = false;
        actionTimeline.stop();
        playButton.setGraphic(new ImageView(imagePlay));
    }

    public void onPlayClicked(ActionEvent actionEvent) {
        if (isFileLoaded()) {
            if (!isPlaying) {
                startAutoPlay();
            } else {
                stopAutoPlay();
            }
        }
    }

    public void onStepClicked(ActionEvent actionEvent) {
        if (isFileLoaded()) {
            executeNext();
        }
    }

    public void onResetClicked(ActionEvent actionEvent) {
        if (isFileLoaded()) {
            Model.getInstance().reset();
            stopAutoPlay();
        }
    }

    private void executeNext() {
        IAction action = Model.getInstance().getNextAction();

        if (action == null) {
            // Execution has finished, stop the timer!
            stopAutoPlay();
            return;
        }

        Command command = Command.fromAction(action);

        assert command != null;
        command.execute(animationController);
    }

    private void setButtonIcons() {
        imagePlay = new Image(getClass().getResourceAsStream("/images/ic_play_arrow_black_24dp.png"));
        imageStop = new Image(getClass().getResourceAsStream("/images/ic_pause_black_24dp.png"));
        Image imageStep = new Image(getClass().getResourceAsStream("/images/ic_skip_next_black_24dp.png"));
        Image imageReset = new Image(getClass().getResourceAsStream("/images/ic_replay_black_24dp.png"));

        playButton.setGraphic(new ImageView(imagePlay));
        stepButton.setGraphic(new ImageView(imageStep));
        resetButton.setGraphic(new ImageView(imageReset));
    }

    // http://clarkonium.net/2015/07/finding-mono-spaced-fonts-in-javafx/
    private ObservableList<String> getMonoFontFamilyNames() {
        final Text thinText = new Text("1 l"); // note the space
        final Text thickText = new Text("MWX");

        List<String> fontFamilyList = Font.getFamilies();
        List<String> monoFamilyList = new ArrayList<>();

        Font font;

        for (String fontFamilyName : fontFamilyList) {
            font = Font.font(fontFamilyName, FontWeight.NORMAL, FontPosture.REGULAR, 14.0d);

            thinText.setFont(font);
            thickText.setFont(font);

            if (thinText.getLayoutBounds().getWidth() == thickText.getLayoutBounds().getWidth()) {
                monoFamilyList.add(fontFamilyName);
            }
        }

        return FXCollections.observableArrayList(monoFamilyList);
    }

    public void onMenu_Close(ActionEvent actionEvent) {
        Platform.exit();
    }


    public void onMenu_OpenFile(ActionEvent actionEvent) {
        selectAndLoadInputFile();
    }

    public void selectAndLoadInputFile() {
        File selectedFile = new FileDialogBuilder(FileDialogBuilder.FileDialogType.OPEN)
                .setOwner(windowStage)
                .setTitle("Select the zip file with the required files...")
                .addFilter(FileDialogBuilder.FileFilters.ZIP_FILES)
                .show();

        if (selectedFile != null) {
            Model.getInstance().loadFile(selectedFile, animationPane);
            resetCodeArea();
        }
    }

    public Boolean isFileLoaded() {
        if (!Model.getInstance().isFileLoaded()) {
            selectAndLoadInputFile();
            return false;
        } else {
            return Model.getInstance().isFileLoaded();
        }
    }

    @FXML
    public void presenterInput(KeyEvent keyEvent) {
        if (keyEvent.getCode() == KeyCode.RIGHT || keyEvent.getCode() == KeyCode.UP || keyEvent.getCode() == KeyCode.PAGE_UP) {
            if (!isPlaying && !isFileLoaded()) {

            } else if (!isPlaying && isFileLoaded()) {
                startAutoPlay();
            } else {

                if (speedSlider.getValue() < speedSlider.getMax()) {
                    if (speedSlider.getValue() + 1 > speedSlider.getMax()) {
                        speedSlider.adjustValue(speedSlider.getValue() + (speedSlider.getMax() - speedSlider.getValue()));
                    } else {
                        speedSlider.adjustValue(speedSlider.getValue() + 1);
                    }
                }
            }
        } else if (keyEvent.getCode() == KeyCode.LEFT || keyEvent.getCode() == KeyCode.DOWN || keyEvent.getCode() == KeyCode.PAGE_DOWN) {
            if (isFileLoaded()) {
                if (speedSlider.getValue() > speedSlider.getMin()) {
                    if (speedSlider.getValue() - 1 < speedSlider.getMin()) {
                        speedSlider.adjustValue(speedSlider.getValue() - (speedSlider.getValue() - speedSlider.getMin()));
                    } else {
                        speedSlider.adjustValue(speedSlider.getValue() - 1);
                    }
                }
            }
        } else if (keyEvent.getCode() == KeyCode.B) {
            if (isFileLoaded()) {
                if (isPlaying) {
                    stopAutoPlay();
                } else {
                    executeNext();
                }
            }
        }
    }

    public void onMenu_GenerateLog(ActionEvent actionEvent) {
        Stage stage = CompileWindowController.createNewWindow(windowStage);
        stage.showAndWait();
    }

    private Stage createScreenIndicator(Screen screen, int number) {
        Stage stage = new Stage();

        stage.setAlwaysOnTop(true);
        stage.initStyle(StageStyle.TRANSPARENT);

        Rectangle rect = new Rectangle(300, 300);
        rect.setFill(Color.gray(0.20));

        Text text = new Text("" + number);
        text.setStyle("-fx-font-size: 200px;");
        text.setFill(Color.WHITE);

        StackPane root = new StackPane();
        root.getChildren().addAll(rect, text);

        stage.setScene(new Scene(root));

        stage.setX(screen.getBounds().getMaxX() - 350);
        stage.setY(screen.getBounds().getMaxY() - 350);

        stage.show();

        return stage;
    }

    private Optional<Screen> screenSelectionDialog() {
        ArrayList<Stage> indicators = new ArrayList<>();

        for (int i = 0; i < Screen.getScreens().size(); i++) {
            indicators.add(createScreenIndicator(Screen.getScreens().get(i), i+1));
        }

        ChoiceDialog<Integer> dialog = new ChoiceDialog<>(1, IntStream.range(1, Screen.getScreens().size()+1).boxed().collect(Collectors.toList()));
        dialog.initOwner(windowStage);
        dialog.setTitle("Screen selection");
        dialog.setHeaderText("Select a screen to show the program on");
        dialog.setContentText("Selected screen: ");

        Optional<Integer> result = dialog.showAndWait();

        for(Stage indicator : indicators) {
            indicator.close();
        }

        return result.map(idx -> Screen.getScreens().get(idx - 1));
    }

    public void onMenu_CreateProjectorWindow(ActionEvent actionEvent) {
        if(projectorWindowActive) {
            projectorWindow.fireEvent(new WindowEvent(windowStage, WindowEvent.WINDOW_CLOSE_REQUEST)); // projectorWindow.close(); won't work!
            projectorWindow = null;
            projectorWindowActive = false;
            projectorPopout.setText(stringResources.getString("menu.view.projectorWindowOpen"));
        } else {
            Optional<Screen> selectedScreen = screenSelectionDialog();

            if (selectedScreen.isPresent()) {
                Optional<Stage> stage = ProjectorWindowController.createNewWindow(
                        selectedScreen.get(),
                        codeArea,
                        animationPane,
                        (codeArea, animationPane) -> {
                            splitPane.getItems().add(0, codeArea);
                            animationAnchorPane.getChildren().add(animationPane);
                            animationAnchorPane.setVisible(true);
                        }
                );

                if (stage.isPresent()) {
                    animationAnchorPane.setVisible(false);
                    splitPane.getItems().remove(0);
                    projectorWindow = stage.get();
                    projectorWindow.show();
                    projectorWindowActive = true;
                    projectorPopout.setText(stringResources.getString("menu.view.projectorWindowClose"));
                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR, "Error: Stage was null, window could not be created!", ButtonType.OK);
                    alert.showAndWait();
                }
            }
        }
    }

    public void onMenu_FontSize(ActionEvent actionEvent) {
        FontSizeWindow fontSizeWindow = new FontSizeWindow(windowStage);
    }
}
