/*
 * Copyright 2018 Stefan Grünzinger, Florian Schachermair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.runimation.vis.view;

import at.runimation.log.variable.Index;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ArrayView extends BaseView {
    private final String name;

    private final NumberView[] valueViews;
    private final IndexView[] indexViews;

    private final Map<String, IndexPointer> varPointerMapping;

    public ArrayView(String name, int length) {
        super(name);

        this.name = name;

        valueViews = new NumberView[length];
        for (int i = 0; i < length; i++) {
            valueViews[i] = new NumberView(true, this);
        }

        indexViews = new IndexView[length];
        for (int i = 0; i < length; i++) {
            indexViews[i] = new IndexView(i);
        }

        varPointerMapping = new HashMap<>();
    }

    @Override
    public void initContent() {
        super.initContent();

        RowConstraints indexConstraints = new RowConstraints();
        indexConstraints.setVgrow(Priority.ALWAYS);
        indexConstraints.setMinHeight(15.0);
        indexConstraints.setPrefHeight(15.0);
        indexConstraints.setValignment(VPos.CENTER);

        getGridPane().getRowConstraints().add(1, indexConstraints);

        for (int i = 0; i < indexViews.length; i++) {
            getGridPane().add(indexViews[i], i + 1, 1);
        }

        for (NumberView valueView : valueViews) {
            valueView.toFront();
        }
    }

    @Override
    public void addChildren(List<Node> childrenList) {
        childrenList.addAll(Arrays.asList(valueViews));
    }

    public NumberView getNumberView(int index) {
        return valueViews[index];
    }

    public void addIndex(Index index) {
        IndexPointer indexPointer = new IndexPointer(index);
        varPointerMapping.put(index.getVariable(), indexPointer);

        if (getGridPane().getRowConstraints().size() < 2) {
            RowConstraints pointerConstraints = new RowConstraints();
            pointerConstraints.setVgrow(Priority.ALWAYS);
            pointerConstraints.setMinHeight(35.0);
            pointerConstraints.setPrefHeight(35.0);
            pointerConstraints.setValignment(VPos.CENTER);

            getGridPane().getRowConstraints().add(2, pointerConstraints);
        }

        getGridPane().add(indexPointer, 1, 2);
    }

    public void setIndex(Index index, int value) {
        IndexPointer pointer = varPointerMapping.get(index.getVariable());
        pointer.setValue(value);

        GridPane.setConstraints(pointer, value + 1, 2);
    }

    public String getName() {
        return name;
    }

    @Override
    public GridPane getGridPane() {
        return super.getGridPane();
    }

    public int getPointerRow() {
        return 2;
    }

    public IndexPointer getIndexPointer(Index index) {
        return varPointerMapping.get(index.getVariable());
    }
}
