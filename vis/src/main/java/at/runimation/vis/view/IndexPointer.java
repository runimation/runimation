/*
 * Copyright 2018 Stefan Grünzinger, Florian Schachermair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.runimation.vis.view;

import at.runimation.log.variable.Index;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class IndexPointer extends BorderPane {
    private Index index;
    private int value;

    public IndexPointer(Index index) {
        this.index = index;
        this.value = 0;

        double[] points = {
                0.0, 1.5,
                0.0, 0.5,
                0.5, 0.0,
                1.0, 0.5,
                1.0, 1.5
        };

        for (int i = 0; i < points.length; i++) {
            points[i] *= 20;
        }

        Polygon polygon = new Polygon(points);
        polygon.setFill(new Color(0.0392, 0.5882, 0.8275, 1));
        polygon.setStroke(new Color(0.1255, 0.3059, 0.5765, 1));
        polygon.setStrokeWidth(1);

        StackPane pane = new StackPane();
        pane.getChildren().add(polygon);

        setCenter(pane);

        Label label = new Label(index.getVariable());
        label.setTranslateY(5);

        Font oldFont = label.getFont();

        label.setFont(Font.font(oldFont.getName(), FontWeight.BOLD, oldFont.getSize() + 1));
        pane.getChildren().add(label);
    }

    public Index getIndex() {
        return index;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
