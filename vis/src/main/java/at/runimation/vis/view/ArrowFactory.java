/*
 * Copyright 2018 Stefan Grünzinger, Florian Schachermair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.runimation.vis.view;

import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import org.reactfx.value.Val;

import java.util.function.IntFunction;

public class ArrowFactory implements IntFunction<Node> {
    private final ObservableValue<Number> currentLine;

    public ArrowFactory(ObservableValue<Number> currentLine) {
        this.currentLine = currentLine;
    }

    @Override
    public Node apply(int line) {
        Polygon triangle = new Polygon(0.0, 0.0, 15.0, 8.0, 0.0, 15.0);
        triangle.setFill(Color.GREEN);
        triangle.setTranslateY(-5.0);

        ObservableValue<Boolean> visible = Val.map(
                currentLine,
                sl -> sl.intValue() == line);

        triangle.visibleProperty().bind(
                Val.flatMap(triangle.sceneProperty(), scene -> scene != null ? visible : Val.constant(false)));

        return triangle;
    }
}
