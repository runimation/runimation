/*
 * Copyright 2018 Stefan Grünzinger, Florian Schachermair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.runimation.vis.view;

import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseView extends Pane {
    public static final double BOX_SIZE = 50.0;
    public static final double MIN_NAME_WIDTH = 70.0;
    public static final double FONT_SIZE = 20.0;

    private final String name;

    private GridPane gridPane;

    public BaseView(String name) {
        this.name = name;
    }

    public void initContent() {
        ColumnConstraints nameConstraints = new ColumnConstraints();
        nameConstraints.setHgrow(Priority.ALWAYS);
        nameConstraints.setMinWidth(MIN_NAME_WIDTH);
        nameConstraints.setHalignment(HPos.CENTER);

        ColumnConstraints contentConstraints = new ColumnConstraints();
        contentConstraints.setHgrow(Priority.ALWAYS);
        contentConstraints.setMinWidth(BOX_SIZE);
        contentConstraints.setPrefWidth(BOX_SIZE);
        contentConstraints.setHalignment(HPos.CENTER);

        RowConstraints rowConstraints = new RowConstraints();
        rowConstraints.setVgrow(Priority.ALWAYS);
        rowConstraints.setMinHeight(BOX_SIZE);
        rowConstraints.setPrefHeight(BOX_SIZE);
        rowConstraints.setValignment(VPos.CENTER);

        gridPane = new GridPane();
        gridPane.getColumnConstraints().add(nameConstraints);
        gridPane.getRowConstraints().add(rowConstraints);

        Text text = new Text(name);
        text.setFont(Font.font(text.getFont().getFamily(), FONT_SIZE));
        text.setTextAlignment(TextAlignment.CENTER);

        gridPane.add(text, 0, 0);

        ArrayList<Node> contentChildren = new ArrayList<>();
        addChildren(contentChildren);

        for (int i = 0; i < contentChildren.size(); i++) {
            gridPane.getColumnConstraints().add(contentConstraints);
            gridPane.add(contentChildren.get(i), i + 1, 0);
        }

        getChildren().add(gridPane);
    }

    public abstract void addChildren(List<Node> childrenList);

    protected GridPane getGridPane() {
        return gridPane;
    }
}