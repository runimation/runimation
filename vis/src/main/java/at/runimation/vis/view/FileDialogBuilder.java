package at.runimation.vis.view;

import javafx.beans.property.SimpleObjectProperty;
import javafx.stage.FileChooser;
import javafx.stage.Window;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FileDialogBuilder {
    private static SimpleObjectProperty<File> lastDirectoryPath = new SimpleObjectProperty<>(new File(System.getProperty("user.home")));

    public enum FileDialogType {
        OPEN, SAVE
    }

    public enum FileFilters {
        ALL_FILES("All files (*.*)", "*.*"),
        JAVA_FILES("Java source files (*.java)", "*.java"),
        ZIP_FILES("Zip files (*.zip)", "*.zip");

        private final FileChooser.ExtensionFilter filter;

        FileFilters(String desc, String... exts) {
            filter = new FileChooser.ExtensionFilter(desc, exts);
        }

        private FileChooser.ExtensionFilter getExtentionFilter() {
            return filter;
        }
    }

    private final FileDialogType fileDialogType;
    private String title = null;
    private List<FileFilters> fileFilters = new ArrayList<>();
    private File initialDirectory = null;
    private Window ownerWindow = null;

    public FileDialogBuilder(FileDialogType type) {
        this.fileDialogType = type;
    }

    public FileDialogBuilder setTitle(String title) {
        this.title = title;
        return this;
    }

    public FileDialogBuilder addFilter(FileFilters filter) {
        fileFilters.add(filter);
        return this;
    }

    public FileDialogBuilder setInitialDirectory(File initialDirectory) {
        this.initialDirectory = initialDirectory;
        return this;
    }

    public FileDialogBuilder setOwner(Window ownerWindow) {
        this.ownerWindow = ownerWindow;
        return this;
    }

    public File show() {
        FileChooser instance = new FileChooser();

        if(initialDirectory != null)
            instance.setInitialDirectory(initialDirectory);
        else
            instance.initialDirectoryProperty().bindBidirectional(lastDirectoryPath);

        instance.getExtensionFilters().setAll(fileFilters.stream()
                .map(FileFilters::getExtentionFilter)
                .collect(Collectors.toList())
        );

        if(title != null)
            instance.setTitle(title);

        File selectedFile = null;

        switch (fileDialogType) {
            case OPEN:
                selectedFile = instance.showOpenDialog(ownerWindow);
                break;
            case SAVE:
                selectedFile = instance.showSaveDialog(ownerWindow);
                break;
        }

        if(selectedFile != null)
            lastDirectoryPath.setValue(selectedFile.getParentFile());

        return selectedFile;
    }
}
