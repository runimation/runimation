/*
 * Copyright 2018 Stefan Grünzinger, Florian Schachermair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.runimation.vis.view;

import javafx.scene.Node;

import java.util.List;

public class VariableView extends BaseView {
    private final NumberView valueView;

    public VariableView(String name) {
        super(name);

        valueView = new NumberView(false, this);
    }

    @Override
    public void addChildren(List<Node> childrenList) {
        childrenList.add(valueView);
    }

    public NumberView getNumberView() {
        return valueView;
    }
}
