/*
 * Copyright 2018 Stefan Grünzinger, Florian Schachermair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.runimation.vis.view.transition;

import javafx.animation.Interpolator;
import javafx.animation.Transition;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.util.Duration;

public class ColorTransition extends Transition {
    private final BorderPane DEFAULT_NODE = null;
    private final double DEFAULT_FROM_R = 0.0;
    private final double DEFAULT_FROM_G = 0.0;
    private final double DEFAULT_FROM_B = 0.0;
    private final double DEFAULT_TO_R = 0.0;
    private final double DEFAULT_TO_G = 0.0;
    private final double DEFAULT_TO_B = 0.0;
    private ObjectProperty<BorderPane> node;
    private DoubleProperty fromR;
    private DoubleProperty fromG;
    private DoubleProperty fromB;
    private DoubleProperty toR;
    private DoubleProperty toG;
    private DoubleProperty toB;

    public ColorTransition(Duration duration) {
        setCycleDuration(duration);
        setInterpolator(Interpolator.EASE_IN);
    }

    public ColorTransition(Duration duration, BorderPane node) {
        this(duration);

        setNode(node);
    }

    public ObjectProperty<BorderPane> nodeProperty() {
        if (node == null) {
            node = new SimpleObjectProperty<>(this, "node", DEFAULT_NODE);
        }

        return node;
    }

    public BorderPane getNode() {
        return this.node == null ? DEFAULT_NODE : this.node.get();
    }

    public void setNode(BorderPane value) {
        if (node != null || value != null) {
            nodeProperty().set(value);

            if (value.getBorder() != null && value.getBorder().getStrokes().size() > 0 && value.getBorder().getStrokes().get(0).getBottomStroke() instanceof Color) {
                Color borderColor = (Color) value.getBorder().getStrokes().get(0).getBottomStroke();
                setFromR(borderColor.getRed());
                setFromG(borderColor.getGreen());
                setFromB(borderColor.getBlue());
            }
        }
    }

    public DoubleProperty fromRProperty() {
        if (fromR == null) {
            fromR = new SimpleDoubleProperty(this, "fromR", 0.0);
        }

        return fromR;
    }

    public double getFromR() {
        return fromR == null ? DEFAULT_FROM_R : fromR.get();
    }

    public void setFromR(double value) {
        if (fromR != null || !Double.isNaN(value)) {
            fromRProperty().set(value);
        }
    }

    public DoubleProperty fromGProperty() {
        if (fromG == null) {
            fromG = new SimpleDoubleProperty(this, "fromG", 0.0);
        }

        return fromG;
    }

    public double getFromG() {
        return fromG == null ? DEFAULT_FROM_G : fromG.get();
    }

    public void setFromG(double value) {
        if (fromG != null || !Double.isNaN(value)) {
            fromGProperty().set(value);
        }
    }

    public DoubleProperty fromBProperty() {
        if (fromB == null) {
            fromB = new SimpleDoubleProperty(this, "fromB", 0.0);
        }

        return fromB;
    }

    public double getFromB() {
        return fromB == null ? DEFAULT_FROM_B : fromB.get();
    }

    public void setFromB(double value) {
        if (fromB != null || !Double.isNaN(value)) {
            fromBProperty().set(value);
        }
    }

    public DoubleProperty toRProperty() {
        if (toR == null) {
            toR = new SimpleDoubleProperty(this, "toR", 0.0);
        }

        return toR;
    }

    public double getToR() {
        return toR == null ? DEFAULT_TO_R : toR.get();
    }

    public void setToR(double value) {
        if (toR != null || !Double.isNaN(value)) {
            toRProperty().set(value);
        }
    }

    public DoubleProperty toGProperty() {
        if (toG == null) {
            toG = new SimpleDoubleProperty(this, "toG", 0.0);
        }

        return toG;
    }

    public double getToG() {
        return toG == null ? DEFAULT_TO_G : toG.get();
    }

    public void setToG(double value) {
        if (toG != null || !Double.isNaN(value)) {
            toGProperty().set(value);
        }
    }

    public DoubleProperty toBProperty() {
        if (toB == null) {
            toB = new SimpleDoubleProperty(this, "toB", 0.0);
        }

        return toB;
    }

    public double getToB() {
        return toB == null ? DEFAULT_TO_B : toB.get();
    }

    public void setToB(double value) {
        if (toB != null || !Double.isNaN(value)) {
            toBProperty().set(value);
        }
    }

    @Override
    protected void interpolate(double frac) {
        Color vColor = new Color(frac * getToR() + (1 - frac) * getFromR(), frac * getToG() + (1 - frac) * getFromG(), frac * getToB() + (1 - frac) * getFromB(), 1);
        getNode().setBorder(new Border(new BorderStroke(vColor, BorderStrokeStyle.SOLID, new CornerRadii(0.2), new BorderWidths(3))));
    }
}
