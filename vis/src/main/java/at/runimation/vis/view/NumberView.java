/*
 * Copyright 2018 Stefan Grünzinger, Florian Schachermair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.runimation.vis.view;

import at.runimation.vis.gui.GUIConfig;
import javafx.geometry.Insets;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

public class NumberView extends BorderPane {
    private final boolean isArrayElement;
    private final BaseView parentView;
    private Object value;
    private Text valueText;

    public NumberView() {
        this(false, null);
    }

    public NumberView(boolean isArrayElement, BaseView parentView) {
        this.isArrayElement = isArrayElement;
        this.parentView = parentView;
        this.value = null;

        valueText = new Text("-");
        valueText.setFill(Color.BLACK);
        valueText.setTextAlignment(TextAlignment.CENTER);
        valueText.fontProperty().bind(GUIConfig.getInstance().animationFontProperty());

        setCenter(valueText);
        setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
        setBorder(new Border(new BorderStroke(Color.SLATEGRAY, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderStroke.THIN)));

        setSnapToPixel(true);
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
        refresh();
    }

    public boolean isArrayElement() {
        return isArrayElement;
    }

    public BaseView getParentView() {
        return parentView;
    }

    public void refresh() {
        valueText.setText((value == null) ? "-" : value.toString());
    }

    @Override
    public NumberView clone() {
        NumberView newView = new NumberView();

        newView.setValue(getValue());
        newView.setMinSize(getWidth(), getWidth());

        // Position has to be set in respect to the animation pane
        //newView.setTranslateX(getTranslateX());
        //newView.setTranslateY(getTranslateY());

        return newView;
    }
}
