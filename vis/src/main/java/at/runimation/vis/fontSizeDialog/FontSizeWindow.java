package at.runimation.vis.fontSizeDialog;

import at.runimation.vis.gui.GUIConfig;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;

public class FontSizeWindow {
    private DialogPane dialogPane;
    private GridPane grid;
    private Spinner<Integer> codeSpinner;
    private Spinner<Integer> varSpinner;

    public FontSizeWindow(Window ownerWindow) {
        codeSpinner = new Spinner<>(8, 500, (int)GUIConfig.getInstance().codeFontProperty().getValue().getSize());
        codeSpinner.setEditable(true);
        varSpinner = new Spinner<>(8, 500, (int)GUIConfig.getInstance().animationFontProperty().getValue().getSize());
        varSpinner.setEditable(true);

        Label codeLabel = new Label("Code: ");
        Label varLabel = new Label("Variables: ");

        grid = new GridPane();

        grid.add(codeLabel, 0, 0);
        grid.add(codeSpinner, 1, 0);

        grid.add(varLabel, 0, 1);
        grid.add(varSpinner, 1, 1);

        grid.getRowConstraints().add(0, new RowConstraints());
        grid.getRowConstraints().add(1, new RowConstraints());

        grid.getRowConstraints().get(0).setVgrow(Priority.SOMETIMES);
        grid.getRowConstraints().get(0).setFillHeight(true);
        grid.getRowConstraints().get(1).setVgrow(Priority.SOMETIMES);
        grid.getRowConstraints().get(1).setFillHeight(true);

        GridPane.setMargin(codeSpinner, new Insets(5.0));
        GridPane.setMargin(varSpinner, new Insets(5.0));

        dialogPane = new DialogPane();
        dialogPane.setContent(grid);
        dialogPane.getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);

        Stage stage = new Stage();

        Button okButton = (Button)dialogPane.lookupButton(ButtonType.OK);
        okButton.setOnAction(event -> {
            int size = codeSpinner.getValue();
            Font oldFont = GUIConfig.getInstance().codeFontProperty().getValue();
            GUIConfig.getInstance().codeFontProperty().setValue(Font.font(oldFont.getFamily(), size));

            size = varSpinner.getValue();
            oldFont = GUIConfig.getInstance().animationFontProperty().getValue();
            GUIConfig.getInstance().animationFontProperty().setValue(Font.font(oldFont.getFamily(), size));

            stage.close();
        });

        Button cancelButton = (Button)dialogPane.lookupButton(ButtonType.CANCEL);
        cancelButton.setOnAction(event -> {
            stage.close();
        });

        stage.setScene(new Scene(dialogPane));
        stage.initOwner(ownerWindow);
        stage.initModality(Modality.WINDOW_MODAL);
        stage.setTitle("Font size settings");
        stage.showAndWait();
    }
}
