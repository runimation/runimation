package at.runimation.vis.transaction;

public interface ITransaction {
    Object getValue();

    void assignFrom(ITransaction other);
    void swapWith(ITransaction other);

    boolean isSwappable();

    static ITransaction fromConstant(Object constant) {
        return new ConstTransaction(constant);
    }
}
