package at.runimation.vis.transaction;

import at.runimation.vis.Model;

public class ElemTransaction implements ITransaction {
    private final String name;
    private final int index;

    public ElemTransaction(String name, int index) {
        this.name = name;
        this.index = index;
    }

    @Override
    public Object getValue() {
        return Model.getInstance().getArrValue(name, index);
    }

    @Override
    public void assignFrom(ITransaction other) {
        Model.getInstance().setArrValue(name, index, other.getValue());
    }

    @Override
    public void swapWith(ITransaction other) {
        if(!other.isSwappable())
            throw new RuntimeException("Other Transaction is not swappable!");

        // Save the current element value and assign the value of other
        Object tmp = Model.getInstance().getArrValue(name, index);
        Model.getInstance().setArrValue(name, index, other.getValue());

        // Assign the saved value to other
        other.assignFrom(ITransaction.fromConstant(tmp));
    }

    @Override
    public boolean isSwappable() {
        return true;
    }
}
