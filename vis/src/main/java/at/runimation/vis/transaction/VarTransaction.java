package at.runimation.vis.transaction;

import at.runimation.vis.Model;

public class VarTransaction implements ITransaction {
    private final String name;

    public VarTransaction(String name) {
        this.name = name;
    }

    @Override
    public Object getValue() {
        return Model.getInstance().getVarValue(name);
    }

    @Override
    public void assignFrom(ITransaction other) {
        Model.getInstance().setVarValue(name, other.getValue());
    }

    @Override
    public void swapWith(ITransaction other) {
        if(!other.isSwappable())
            throw new RuntimeException("Other Transaction is not swappable!");

        // Save the current element value and assign the value of other
        Object tmp = Model.getInstance().getVarValue(name);
        Model.getInstance().setVarValue(name, other.getValue());

        // Assign the saved value to other
        other.assignFrom(ITransaction.fromConstant(tmp));
    }

    @Override
    public boolean isSwappable() {
        return true;
    }
}
