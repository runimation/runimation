package at.runimation.vis.transaction;

public class ConstTransaction implements ITransaction {

    private final Object value;

    public ConstTransaction(Object value) {
        this.value = value;
    }

    @Override
    public Object getValue() {
        return value;
    }

    @Override
    public void assignFrom(ITransaction other) {
        throw new IllegalStateException("Cannot assign anything on a constant transaction");
    }

    @Override
    public void swapWith(ITransaction other) {
        throw new IllegalStateException("Cannot swap with a constant!");
    }

    @Override
    public boolean isSwappable() {
        return false;
    }
}
