/*
 * Copyright 2018 Stefan Grünzinger, Florian Schachermair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.runimation.vis;

import at.runimation.log.LogIn;
import at.runimation.log.action.IAction;
import at.runimation.log.action.LinedAction;
import at.runimation.log.variable.ArrayVariable;
import at.runimation.log.variable.IVariable;
import at.runimation.log.variable.Index;
import at.runimation.vis.controls.AnimationPane;
import at.runimation.vis.view.ArrayView;
import at.runimation.vis.view.BaseView;
import at.runimation.vis.view.NumberView;
import at.runimation.vis.view.VariableView;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.geometry.Insets;
import javafx.scene.control.Separator;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public final class Model {
    public static Model getInstance() {
        return ModelSingletonHolder.INSTANCE;
    }

    private Map<String, Object> values = new HashMap<>();
    private Map<String, Index> indices = new HashMap<>();
    private Map<String, BaseView> views = new HashMap<>();

    private int nextActionNumber;
    private IAction nextAction;

    private boolean fileLoaded = false;

    private Model() {
    }

    private IntegerProperty nextLine;
    public IntegerProperty nextLineProperty() {
        if (nextLine == null) {
            nextLine = new SimpleIntegerProperty();
        }

        return nextLine;
    }

    public void loadFile(File zipFile, AnimationPane animationPane) {
        if (!LogIn.loadFromZip(zipFile)) {
            System.err.println("Error loading zip file!");
            return;
        }

        loadVariables(animationPane);
        loadIndices();

        reset();

        fileLoaded = true;
    }

    private void loadVariables(AnimationPane animationPane) {
        values.clear();
        indices.clear();
        views.clear();

        animationPane.getVariablePane().getChildren().clear();

        for (int i = 0; i < LogIn.getNumberVariables(); i++) {
            IVariable variable = LogIn.getVariable(i);

            BaseView view = null;
            switch (variable.getKind()) {
                case SIMPLE:
                    // Don't have to initialize variable contents
                    view = new VariableView(variable.getName());
                    break;
                case ARRAY:
                    // Initialize array with null
                    values.put(variable.getName(), new Object[((ArrayVariable) variable).getLength()]);
                    view = new ArrayView(variable.getName(), ((ArrayVariable) variable).getLength());
                    break;
            }

            views.put(variable.getName(), view);
            animationPane.getVariablePane().getChildren().add(view);

            Separator separator = new Separator();
            separator.setPadding(new Insets(5.0, 0.0, 5.0, 0.0));
            animationPane.getVariablePane().getChildren().add(separator);

            view.initContent();
        }
    }

    private void loadIndices() {
        for (int i = 0; i < LogIn.getNumberIndices(); i++) {
            Index idx = LogIn.getIndex(i);

            indices.put(idx.getVariable(), idx);

            ArrayView arrayView = getView(idx.getArray());
            arrayView.addIndex(idx);
        }
    }

    public void reset() {
        for (Map.Entry<String, BaseView> viewEntry : views.entrySet()) {
            BaseView baseView = viewEntry.getValue();

            if (baseView instanceof VariableView) {
                ((VariableView) baseView).getNumberView().setValue(null);
            } else if (baseView instanceof ArrayView) {
                Object[] arr = (Object[]) values.get(viewEntry.getKey());

                for (int i = 0; i < arr.length; i++) {
                    ((ArrayView) baseView).getNumberView(i).setValue(null);
                }
            }
        }

        for (Map.Entry<String, Index> indexEntry : indices.entrySet()) {
            ArrayView arrayView = getView(indexEntry.getValue().getArray());

            arrayView.setIndex(indexEntry.getValue(), 0);
        }

        nextActionNumber = 1;
        nextAction = LogIn.getAction(0);
    }

    public IAction getNextAction() {
        if (nextActionNumber < LogIn.getNumberActions()) {
            IAction action = nextAction;

            nextAction = LogIn.getAction(nextActionNumber);
            nextActionNumber++;

            nextLineProperty().setValue(((LinedAction) nextAction).getLine());

            return action;
        }

        return null;
    }

    public boolean isFileLoaded() {
        return fileLoaded;
    }

    public String getSourceCode() {
        if (!fileLoaded) {
            return "No file loaded!";
        } else {
            return LogIn.getSource();
        }
    }

    public Object getVarValue(String name) {
        return values.get(name);
    }

    public Object getArrValue(String name, int index) {
        return ((Object[]) values.get(name))[index];
    }

    public void setVarValue(String name, Object value) {
        values.put(name, value);

        if(isVariableIndex(name)) {
            updateIndex(name, value);
        }
    }

    public void setArrValue(String name, int index, Object value) {
        Object[] temp = (Object[]) values.get(name);
        temp[index] = value;
        values.put(name, temp);
    }

    public void copyVarValue(String var1, String var2) {
        values.put(var1, values.get(var2));
    }

    @SuppressWarnings("unchecked")
    public <T extends BaseView> T getView(String name) {
        return (T)views.get(name);
    }

    public NumberView getNumberView(String name) {
        return ((VariableView) views.get(name)).getNumberView();
    }

    public NumberView getNumberView(String name, int index) {
        return ((ArrayView) views.get(name)).getNumberView(index);
    }

    private boolean isVariableIndex(String name) {
        return indices.containsKey(name);
    }

    private void updateIndex(String name, Object value) {
        Index index = indices.get(name);

        ArrayView arrayView = getView(index.getArray());
        arrayView.setIndex(index, (int) value);
    }

    private static class ModelSingletonHolder {
        static final Model INSTANCE = new Model();
    }
}
