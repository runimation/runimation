/*
 * Copyright 2018 Stefan Grünzinger, Florian Schachermair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.runimation.vis.controls;

import at.runimation.vis.Util;
import javafx.geometry.Orientation;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;

public class AnimationPane extends TitledPane {
    private final FlowPane variablePane;
    private final Pane animationPane;

    public AnimationPane() {
        animationPane = new Pane();
        Util.setAllAnchors(animationPane, 0.0D);
        AnchorPane animationAnchor = new AnchorPane(animationPane);

        variablePane = new FlowPane();
        variablePane.setOrientation(Orientation.VERTICAL);

        StackPane stackPane = new StackPane(animationAnchor, variablePane);
        Util.setAllAnchors(stackPane, 0.0D);

        setContent(new AnchorPane(stackPane));

        setCollapsible(false);
    }

    public FlowPane getVariablePane() {
        return variablePane;
    }

    public Pane getAnimationPane() {
        return animationPane;
    }
}
