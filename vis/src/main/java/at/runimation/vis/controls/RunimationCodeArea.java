/*
 * Copyright 2018 Stefan Grünzinger, Florian Schachermair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.runimation.vis.controls;

import at.runimation.vis.gui.GUIConfig;
import at.runimation.vis.JavaHighlighter;
import at.runimation.vis.Model;
import at.runimation.vis.view.ArrowFactory;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import org.fxmisc.flowless.VirtualizedScrollPane;
import org.fxmisc.richtext.CodeArea;
import org.fxmisc.richtext.LineNumberFactory;

import java.util.function.IntFunction;

public class RunimationCodeArea extends AnchorPane {
    private final CodeArea codeArea;

    public RunimationCodeArea() {
        super();

        codeArea = new CodeArea();
        codeArea.setEditable(false);

        IntFunction<Node> lineNumberFactory = LineNumberFactory.get(codeArea);
        IntFunction<Node> arrowFactory = new ArrowFactory(Model.getInstance().nextLineProperty());
        IntFunction<Node> graphicFactory = line -> {
            Label numberLabel = (Label) lineNumberFactory.apply(line);
            Node arrow = arrowFactory.apply(line);

            BorderPane arrowPane = new BorderPane();
            arrowPane.setCenter(arrow);
            arrowPane.setMinWidth(arrow.getBoundsInLocal().getWidth() + 10.0);

            HBox hbox = new HBox(numberLabel, arrowPane);
            hbox.setAlignment(Pos.CENTER_LEFT);
            hbox.setBackground(numberLabel.getBackground());

            return hbox;
        };

        codeArea.setParagraphGraphicFactory(graphicFactory);
        codeArea.setPrefWidth(200.0);
        codeArea.setPrefHeight(100.0);

        GUIConfig.getInstance().codeFontProperty().addListener((observable, oldFont, newFont) -> {
            // Set code area font and font size
            codeArea.setStyle(String.format("-fx-font-family: %s; -fx-font-size: %fpt", newFont.getName(), newFont.getSize()));
        });

        VirtualizedScrollPane<CodeArea> vsp = new VirtualizedScrollPane<>(codeArea);

        vsp.setHbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        vsp.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);

        getChildren().add(vsp);
        AnchorPane.setBottomAnchor(vsp, 0.0);
        AnchorPane.setTopAnchor(vsp, 0.0);
        AnchorPane.setLeftAnchor(vsp, 0.0);
        AnchorPane.setRightAnchor(vsp, 0.0);
    }

    public void setText(String text) {
        codeArea.replaceText(0, codeArea.getText().length(), text);
    }

    public void setSourceCode(String sourceCode) {
        setText(sourceCode);
        codeArea.setStyleSpans(0, JavaHighlighter.computeHighlighting(sourceCode));
    }
}
