/*
 * Copyright 2018 Stefan Grünzinger, Florian Schachermair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.runimation.vis.animation;

import at.runimation.vis.gui.GUIConfig;
import at.runimation.vis.controls.AnimationPane;
import javafx.scene.layout.Pane;
import javafx.util.Duration;

public final class AnimationController {
    private final AnimationPane animationPane;
    private Animation currentAnimation;

    public AnimationController(AnimationPane animationPane) {
        this.animationPane = animationPane;
    }

    Pane getAnimationPane() {
        return animationPane.getAnimationPane();
    }

    public void startAnimation(Animation animation) {
        checkCurrentAnimation();
        currentAnimation = animation;
        animationPane.getAnimationPane().getChildren().clear();
        animation.startAnimation(this, new Duration(GUIConfig.getInstance().getDuration().multiply(0.7).toMillis()));
    }

    public void startWithoutAnimation(Animation animation) {
        checkCurrentAnimation();
        currentAnimation = animation;
        animation.changeValues();
    }

    private void checkCurrentAnimation() {
        if (currentAnimation != null) {
            currentAnimation.stopAnimation();
        }

    }
}
