/*
 * Copyright 2018 Stefan Grünzinger, Florian Schachermair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.runimation.vis.animation;

import at.runimation.vis.view.BaseView;
import at.runimation.vis.view.NumberView;
import javafx.animation.ParallelTransition;
import javafx.animation.Transition;
import javafx.animation.TranslateTransition;
import javafx.geometry.Bounds;
import javafx.scene.layout.Pane;
import javafx.util.Duration;

public class SwapAnimation extends AnimationBase {
    private final NumberView firstView;
    private final NumberView secondView;

    public SwapAnimation(NumberView firstView, NumberView secondView) {
        this.firstView = firstView;
        this.secondView = secondView;
    }

    @Override
    public Transition createTransition(AnimationController controller, Pane animationPane, Duration duration) {
        Object firstValue = firstView.getValue();
        Object secondValue = secondView.getValue();

        Bounds firstBounds = animationPane.sceneToLocal(firstView.localToScene(firstView.getBoundsInLocal()));
        Bounds secondBounds = animationPane.sceneToLocal(secondView.localToScene(secondView.getBoundsInLocal()));

        NumberView animationViewOne = new NumberView();

        animationViewOne.setMinSize(BaseView.BOX_SIZE, BaseView.BOX_SIZE);
        animationViewOne.setValue(secondValue);

        animationViewOne.toFront();
        animationViewOne.setTranslateX(secondBounds.getMinX());
        animationViewOne.setTranslateY(secondBounds.getMinY());

        animationPane.getChildren().add(animationViewOne);
        animationPane.getParent().toFront();

        firstView.setVisible(false);

        NumberView animationViewTwo = new NumberView();

        animationViewTwo.setMinSize(BaseView.BOX_SIZE, BaseView.BOX_SIZE);
        animationViewTwo.setValue(firstValue);

        animationViewTwo.toFront();
        animationViewTwo.setTranslateX(firstBounds.getMinX());
        animationViewTwo.setTranslateY(firstBounds.getMinY());

        animationPane.getChildren().add(animationViewTwo);
        animationPane.getParent().toFront();

        secondView.setVisible(false);

        TranslateTransition t1 = new TranslateTransition(duration, animationViewOne);

        t1.setToX(firstBounds.getMinX());
        t1.setToY(firstBounds.getMinY());

        t1.setFromX(secondBounds.getMinX());
        t1.setFromY(secondBounds.getMinY());

        t1.setOnFinished(event -> {
            firstView.setValue(secondValue);
            animationPane.getChildren().remove(animationViewOne);
        });

        TranslateTransition t2 = new TranslateTransition(duration, animationViewTwo);

        t2.setToX(secondBounds.getMinX());
        t2.setToY(secondBounds.getMinY());

        t2.setFromX(firstBounds.getMinX());
        t2.setFromY(firstBounds.getMinY());

        t2.setOnFinished(event -> {
            secondView.setValue(firstValue);
            animationPane.getChildren().remove(animationViewTwo);
        });

        ParallelTransition parallelTransition = new ParallelTransition(t1, t2);

        parallelTransition.setOnFinished(event -> {
            firstView.setVisible(true);
            secondView.setVisible(true);
        });

        return parallelTransition;
    }

    @Override
    public void changeValues() {
        Object firstValue = firstView.getValue();
        Object secondValue = secondView.getValue();

        firstView.setValue(secondValue);
        secondView.setValue(firstValue);
    }
}
