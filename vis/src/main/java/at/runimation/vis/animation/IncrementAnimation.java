/*
 * Copyright 2018 Stefan Grünzinger, Florian Schachermair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.runimation.vis.animation;

import at.runimation.vis.view.BaseView;
import at.runimation.vis.view.NumberView;
import javafx.animation.RotateTransition;
import javafx.animation.SequentialTransition;
import javafx.animation.Transition;
import javafx.geometry.Bounds;
import javafx.scene.layout.Pane;
import javafx.scene.transform.Rotate;
import javafx.util.Duration;

public class IncrementAnimation extends AnimationBase {
    private final NumberView view;
    private final Object value;

    public IncrementAnimation(NumberView view, Object value) {
        this.view = view;
        this.value = value;
    }

    @Override
    protected Transition createTransition(AnimationController controller, Pane animationPane, Duration duration) {
        Bounds bounds = animationPane.sceneToLocal(view.localToScene(view.getBoundsInLocal()));

        NumberView animationView = new NumberView();
        animationView.setValue(view.getValue());
        animationView.setTranslateX(bounds.getMinX());
        animationView.setTranslateY(bounds.getMinY());
        animationView.setMinSize(BaseView.BOX_SIZE, BaseView.BOX_SIZE);

        animationPane.getChildren().add(animationView);
        view.setVisible(false);
        view.setValue(value);

        RotateTransition t1 = new RotateTransition(duration.divide(2), animationView);
        t1.setFromAngle(0);
        t1.setToAngle(90);
        t1.setCycleCount(1);
        t1.setAxis(Rotate.X_AXIS);
        t1.setOnFinished(event -> animationView.setValue(value));

        RotateTransition t2 = new RotateTransition(duration.divide(2), animationView);
        t2.setFromAngle(-90);
        t2.setToAngle(0);
        t2.setCycleCount(1);
        t2.setAxis(Rotate.X_AXIS);

        SequentialTransition transition = new SequentialTransition(t1, t2);
        transition.setOnFinished(event -> {
            animationPane.getChildren().remove(animationView);
            view.setVisible(true);
        });

        return transition;
    }

    @Override
    public void changeValues() {
        view.setValue(value);
    }
}
