/*
 * Copyright 2018 Stefan Grünzinger, Florian Schachermair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.runimation.vis.animation;

import at.runimation.vis.view.ArrayView;
import at.runimation.vis.view.BaseView;
import at.runimation.vis.view.NumberView;
import at.runimation.vis.view.transition.ColorTransition;
import javafx.animation.*;
import javafx.geometry.Bounds;
import javafx.scene.Node;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.util.Duration;

public class CompareAnimation extends AnimationBase {
    private final NumberView firstView;
    private final Object firstValue;
    private final boolean isFirstConst;

    private final NumberView secondView;
    private final Object secondValue;
    private final boolean isSecondConst;

    private final String compareType;
    private final boolean result;

    private final ArrayView firstArray;
    private final ArrayView secondArray;

    private final boolean isFirstElement;
    private final boolean isSecondElement;

    private CompareAnimation(NumberView firstView, Object firstValue, boolean isFirstConst, NumberView secondView, Object secondValue, boolean isSecondConst, String compareType, boolean result, ArrayView firstArray, ArrayView secondArray, boolean isFirstElement, boolean isSecondElement) {
        this.firstView = firstView;
        this.firstValue = firstValue;
        this.isFirstConst = isFirstConst;
        this.secondView = secondView;
        this.secondValue = secondValue;
        this.isSecondConst = isSecondConst;
        this.compareType = compareType;
        this.result = result;
        this.firstArray = firstArray;
        this.secondArray = secondArray;
        this.isFirstElement = isFirstElement;
        this.isSecondElement = isSecondElement;
    }

    @Override
    public Transition createTransition(AnimationController controller, Pane animationPane, Duration duration) {
        double animateX = calcAnimationX(animationPane);
        double animateY = calcAnimationY(animationPane);

        NumberView animationView1 = getAnimationView(isFirstConst, firstView, firstValue, animationPane);
        NumberView animationView2 = getAnimationView(isSecondConst, secondView, secondValue, animationPane);

        BorderPane compareView = (new BorderPane(new Text(compareType)));
        compareView.setMinSize(BaseView.BOX_SIZE, BaseView.BOX_SIZE);
        compareView.setTranslateX(animateX + BaseView.BOX_SIZE);
        compareView.setTranslateY(animateY);
        compareView.setScaleX(0.0);
        compareView.setScaleY(0.0);
        animationPane.getChildren().add(compareView);

        Transition t1 = getNumberViewTransition(isFirstConst, animationView1, duration.divide(4), animateX, animateY);
        Transition t2 = getNumberViewTransition(isSecondConst, animationView2, duration.divide(4), animateX + animationView1.getMinWidth() + compareView.getMinWidth(), animateY);

        ScaleTransition compareTypeAnimation = new ScaleTransition(duration.divide(4), compareView);
        compareTypeAnimation.setFromX(0);
        compareTypeAnimation.setFromY(0);
        compareTypeAnimation.setToX(1);
        compareTypeAnimation.setToY(1);

        ColorTransition resultColorFade1 = new ColorTransition(duration.divide(4), animationView1);
        ColorTransition resultColorFade2 = new ColorTransition(duration.divide(4), animationView2);

        if (result) {
            resultColorFade1.setToG(0.7);
            resultColorFade2.setToG(0.7);
        } else {
            resultColorFade1.setToR(1.0);
            resultColorFade2.setToR(1.0);
        }

        ParallelTransition parallelTransition = new ParallelTransition(t1, t2, compareTypeAnimation);

        SequentialTransition sequentialTransition = new SequentialTransition(parallelTransition, new ParallelTransition(resultColorFade1, resultColorFade2), new PauseTransition(duration.divide(2)));
        sequentialTransition.setOnFinished(event -> {
            animationPane.getChildren().remove(animationView1);
            animationPane.getChildren().remove(animationView2);
            animationPane.getChildren().remove(compareView);
        });

        animationPane.getChildren().add(animationView1);
        animationPane.getChildren().add(animationView2);
        animationPane.getParent().toFront();

        return sequentialTransition;
    }

    public Bounds getAnimationBounds(Pane animationPane, Node view) {
        return animationPane.sceneToLocal(view.localToScene(view.getLayoutBounds()));
    }

    public double calcAnimationY(Pane animationPane) {
        if (!isFirstConst && !isSecondConst) {
            return (getAnimationBounds(animationPane, firstView).getMinY() + getAnimationBounds(animationPane, secondView).getMinY()) / 2;
        } else if (!isFirstConst) {
            return getAnimationBounds(animationPane, firstView).getMinY();
        } else if (!isSecondConst) {
            return getAnimationBounds(animationPane, secondView).getMinY();
        }

        return 0;
    }

    private double calcAnimationX(Pane animationPane) {
        if (!isFirstConst && !isSecondConst) {
            return Math.max(Math.max(getViewX(animationPane, firstView, firstArray), getViewX(animationPane, secondView, secondArray)), 250.0);
        } else if (!isFirstConst) {
            return Math.max(getViewX(animationPane, firstView, firstArray), 100.0);
        } else if (!isSecondConst) {
            return Math.max(getViewX(animationPane, secondView, secondArray), 100.0);
        }

        return 0;
    }

    private double getViewX(Pane animationPane, NumberView view, ArrayView arrayView) {
        if (view.isArrayElement()) {
            return getAnimationBounds(animationPane, arrayView).getMaxX() + view.getWidth();
        }

        return getAnimationBounds(animationPane, view).getMaxX() + view.getWidth();
    }

    private NumberView getAnimationView(boolean isConst, NumberView view, Object value, Pane animationPane) {
        if (isConst) {
            NumberView animationView = new NumberView();

            animationView.setValue(value);
            animationView.setMinSize(BaseView.BOX_SIZE, BaseView.BOX_SIZE);

            return animationView;
        }

        Bounds animationBounds = getAnimationBounds(animationPane, view);

        NumberView animationView = view.clone();
        animationView.setTranslateX(animationBounds.getMinX());
        animationView.setTranslateY(animationBounds.getMinY());

        return animationView;
    }

    private Transition getNumberViewTransition(boolean isConst, NumberView animationView, Duration duration, double animateX, double animateY) {
        if (isConst) {
            animationView.setTranslateX(animateX);
            animationView.setTranslateY(animateY);

            animationView.setScaleX(0.0);
            animationView.setScaleY(0.0);

            ScaleTransition scaleTransition = new ScaleTransition(duration, animationView);
            scaleTransition.setToX(1.0);
            scaleTransition.setToY(1.0);

            return scaleTransition;
        } else {
            TranslateTransition translateTransition = new TranslateTransition(duration, animationView);
            translateTransition.setFromX(animationView.getTranslateX());
            translateTransition.setFromY(animationView.getTranslateY());
            translateTransition.setToX(animateX);
            translateTransition.setToY(animateY);

            return translateTransition;
        }
    }

    @Override
    public void changeValues() {
    }

    public static class Builder {
        private NumberView firstView;
        private Object firstValue;
        private boolean isFirstConst;

        private NumberView secondView;
        private Object secondObject;
        private boolean isSecondConst;

        private String compareType;
        private boolean result;

        private ArrayView firstArray;
        private ArrayView secondArray;

        private boolean isFirstElement;
        private boolean isSecondElement;

        public Builder() {
            firstView = null;
            firstValue = null;
            isSecondConst = true;
            secondView = null;
            secondObject = null;
            isSecondConst = true;
            compareType = null;
            result = false;
            isFirstElement = false;
            firstArray = null;
            isSecondElement = false;
            secondArray = null;
        }

        public Builder setFirst(NumberView firstView) {
            this.firstView = firstView;
            this.firstValue = null;
            this.isFirstConst = false;

            if (firstView.getParentView() instanceof ArrayView) {
                isFirstElement = true;
                firstArray = (ArrayView) firstView.getParentView();
            }

            return this;
        }

        public Builder setFirst(Object firstValue) {
            this.firstView = null;
            this.firstValue = firstValue;
            this.isFirstConst = true;

            return this;
        }

        public Builder setSecond(NumberView secondView) {
            this.secondView = secondView;
            this.secondObject = null;
            this.isSecondConst = false;

            if (secondView.getParentView() instanceof ArrayView) {
                isSecondElement = true;
                secondArray = (ArrayView) secondView.getParentView();
            }

            return this;
        }

        public Builder setSecond(Object secondObject) {
            this.secondView = null;
            this.secondObject = secondObject;
            this.isSecondConst = true;

            return this;
        }

        public Builder setCompareType(String compareType) {
            this.compareType = compareType;

            return this;
        }

        public Builder setResult(boolean result) {
            this.result = result;

            return this;
        }

        public CompareAnimation build() {
            return new CompareAnimation(firstView, firstValue, isFirstConst, secondView, secondObject, isSecondConst, compareType, result, firstArray, secondArray, isFirstElement, isSecondElement);
        }
    }
}
