/*
 * Copyright 2018 Stefan Grünzinger, Florian Schachermair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.runimation.vis.animation;

import at.runimation.vis.view.BaseView;
import at.runimation.vis.view.NumberView;
import javafx.animation.ScaleTransition;
import javafx.animation.Transition;
import javafx.animation.TranslateTransition;
import javafx.geometry.Bounds;
import javafx.scene.layout.Pane;
import javafx.util.Duration;

public class AssignAnimation extends AnimationBase {
    private final NumberView firstView;
    private final NumberView secondView;
    private final Object secondValue;
    private final boolean isSecondConst;

    AssignAnimation(NumberView firstView, NumberView secondView, Object secondValue, boolean isSecondConst) {
        this.firstView = firstView;
        this.secondView = secondView;
        this.secondValue = secondValue;
        this.isSecondConst = isSecondConst;
    }

    @Override
    public Transition createTransition(AnimationController controller, Pane animationPane, Duration duration) {
        if (isSecondConst) {
            Bounds bounds = animationPane.sceneToLocal(firstView.localToScene(firstView.getBoundsInLocal()));

            NumberView animationView = new NumberView();
            animationView.setValue(secondValue);
            animationView.setTranslateX(bounds.getMinX());
            animationView.setTranslateY(bounds.getMinY());
            animationView.setMinSize(BaseView.BOX_SIZE, BaseView.BOX_SIZE);

            animationPane.getChildren().add(animationView);

            firstView.setVisible(false);
            firstView.setValue(secondValue);

            ScaleTransition t1 = new ScaleTransition(duration, animationView);
            t1.setFromX(1.2);
            t1.setFromY(1.2);
            t1.setToX(1.0);
            t1.setToY(1.0);

            t1.setOnFinished(event -> {
                animationPane.getChildren().remove(animationView);
                firstView.setVisible(true);
            });

            return t1;
        } else {
            Bounds firstBounds = animationPane.sceneToLocal(firstView.localToScene(firstView.getBoundsInLocal()));
            Bounds secondBounds = animationPane.sceneToLocal(secondView.localToScene(secondView.getBoundsInLocal()));

            NumberView animationView = new NumberView();

            animationView.setMinSize(BaseView.BOX_SIZE, BaseView.BOX_SIZE);
            animationView.setValue(secondView.getValue());

            animationView.toFront();
            animationView.setTranslateX(secondBounds.getMinX());
            animationView.setTranslateY(secondBounds.getMinY());

            animationPane.getChildren().add(animationView);
            animationPane.getParent().toFront();

            TranslateTransition t = new TranslateTransition(duration, animationView);

            t.setToX(firstBounds.getMinX());
            t.setToY(firstBounds.getMinY());

            t.setFromX(secondBounds.getMinX());
            t.setFromY(secondBounds.getMinY());

            t.setOnFinished(event -> {
                firstView.setValue(secondView.getValue());
                animationPane.getChildren().remove(animationView);
            });

            return t;
        }
    }

    @Override
    public void changeValues() {
        if (isSecondConst) {
            firstView.setValue(secondValue);
        } else {
            firstView.setValue(secondView.getValue());
        }
    }

    private void finished(double x, double y) {
        firstView.getChildren().get(0).setTranslateX(x);
        firstView.getChildren().get(0).setTranslateY(y);
        firstView.setValue(secondView.getValue());
        firstView.refresh();
        firstView.toFront();
    }

    public static class Builder {
        private NumberView firstView = null;
        private NumberView secondView = null;
        private Object secondValue = null;
        private boolean isSecondConst = true;

        public Builder setFirst(NumberView firstView) {
            this.firstView = firstView;
            return this;
        }

        public Builder setSecond(NumberView secondView) {
            this.secondView = secondView;
            this.secondValue = null;
            this.isSecondConst = false;
            return this;
        }

        public Builder setSecond(Object secondValue) {
            this.secondView = null;
            this.secondValue = secondValue;
            this.isSecondConst = true;

            return this;
        }

        public AssignAnimation build() {
            return new AssignAnimation(firstView, secondView, secondValue, isSecondConst);
        }
    }
}
