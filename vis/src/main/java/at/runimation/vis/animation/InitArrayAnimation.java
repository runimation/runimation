/*
 * Copyright 2018 Stefan Grünzinger, Florian Schachermair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.runimation.vis.animation;

import at.runimation.vis.view.ArrayView;
import at.runimation.vis.view.NumberView;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.Transition;
import javafx.scene.layout.Pane;
import javafx.util.Duration;

public class InitArrayAnimation extends AnimationBase {
    private final ArrayView arrayView;
    private final Object[] values;

    public InitArrayAnimation(ArrayView arrayView, Object[] values) {
        this.arrayView = arrayView;
        this.values = values;
    }

    @Override
    public Transition createTransition(AnimationController controller, Pane animationPane, Duration duration) {
        ParallelTransition parTrans = new ParallelTransition();

        for (int i = 0; i < values.length; i++) {
            NumberView numberView = arrayView.getNumberView(i);
            numberView.setValue(values[i]);

            ScaleTransition t1 = new ScaleTransition(duration, numberView);
            t1.setFromX(1.2);
            t1.setFromY(1.2);
            t1.setToX(1.0);
            t1.setToY(1.0);

            parTrans.getChildren().add(t1);
        }

        return parTrans;
    }

    @Override
    public void changeValues() {
        for (int i = 0; i < values.length; i++) {
            NumberView numberView = arrayView.getNumberView(i);
            numberView.setValue(values[i]);
        }
    }
}
