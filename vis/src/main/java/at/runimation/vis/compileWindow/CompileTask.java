package at.runimation.vis.compileWindow;

import at.runimation.compiler.CompileUnit;
import at.runimation.compiler.exception.CompilerException;
import javafx.concurrent.Task;

import java.io.PrintStream;

public class CompileTask extends Task<Void> {
    private final CompileUnit compileUnit;
    private final PrintStream logStream;

    public CompileTask(CompileUnit compileUnit, PrintStream logStream) {
        this.compileUnit = compileUnit;
        this.logStream = logStream;
    }

    @Override
    protected Void call() throws Exception {
        updateProgress(0, 4);

        logStream.println("Stage 1: Compiling instrumentation");
        if(!compileUnit.compileInstrumentation()) {
            logStream.println("Compilation of the instrumentation failed!");
            return null;
        }
        updateProgress(1, 4);

        logStream.println("Stage 2: Compile java code");
        try {
            if(!compileUnit.compileProgram()) {
                logStream.println("Compilation of the java code failed.");
                return null;
            }
        } catch (CompilerException e) {
            e.printStackTrace(logStream);
            return null;
        }
        updateProgress(2, 4);

        logStream.println("Stage 3: Collect data");
        try {
            if (!compileUnit.collectData()) {
                logStream.println("Running the program failed!");
                return null;
            }
        } catch (CompilerException e) {
            e.printStackTrace(logStream);
            return null;
        }
        updateProgress(3, 4);

        logStream.println("Stage 4: Package files into zip");
        compileUnit.packageFiles();
        updateProgress(4, 4);
        logStream.println("Compilation successful!");

        return null;
    }
}
