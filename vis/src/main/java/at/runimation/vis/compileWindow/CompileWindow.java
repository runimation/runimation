package at.runimation.vis.compileWindow;

import at.runimation.vis.gui.BaseWindow;
import at.runimation.vis.gui.WindowOptions;
import javafx.scene.image.Image;
import javafx.stage.Window;

public class CompileWindow extends BaseWindow {
    public CompileWindow(Window owner) {
        super(
                CompileWindow.class.getResource("/compile_window/compile_window.fxml"),
                new WindowOptions()
                        .setTitle("Runimation - Compiler")
                        .setOwner(owner)
                        .setWindowIcon(new Image(CompileWindow.class.getResourceAsStream("/images/logo.png")))
                        .setControllerFactory(CompileWindowController::new)
        );
    }
}
