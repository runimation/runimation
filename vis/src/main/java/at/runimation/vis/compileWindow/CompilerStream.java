package at.runimation.vis.compileWindow;

import javafx.scene.control.TextArea;

import java.io.OutputStream;

public class CompilerStream extends OutputStream {
    private final TextArea textArea;

    public CompilerStream(TextArea textArea) {
        this.textArea = textArea;
    }

    @Override
    public void write(int b) {
        String s = new String(new byte[] { (byte)b });
        textArea.appendText(s);
    }
}
