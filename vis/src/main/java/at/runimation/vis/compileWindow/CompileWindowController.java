/*
 * Copyright 2018 Stefan Grünzinger, Florian Schachermair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.runimation.vis.compileWindow;

import at.runimation.compiler.CompileUnit;
import at.runimation.vis.gui.IWindowController;
import at.runimation.vis.view.FileDialogBuilder;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.Window;

import java.io.File;
import java.io.PrintStream;

public class CompileWindowController implements IWindowController {
    public TextField outputPathField;
    public TextField sourceFilePath;

    public Button closeButton;
    public Button compileButton;
    public ProgressBar compilerProgress;
    public TextArea outputArea;

    private Stage windowStage;

    public static Stage createNewWindow(Window owner) {
        return new CompileWindow(owner);
    }

    @FXML
    void initialize() {
        sourceFilePath.textProperty().addListener((observable, oldValue, newValue) -> {
            if (isSourcePathValid(newValue)) {
                sourceFilePath.setStyle(
                        "-fx-border-color: transparent; " +
                                "-fx-border-width: 2px;" +
                                "-fx-border-style: solid;" +
                                "-fx-border-radius: 2 2 2 2;"
                );
            } else {
                sourceFilePath.setStyle(
                        "-fx-border-color: red; " +
                                "-fx-border-width: 2px;" +
                                "-fx-border-style: solid;" +
                                "-fx-border-radius: 2 2 2 2;"
                );
            }
        });

        outputPathField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (isOutputPathValid(newValue)) {
                outputPathField.setStyle(
                        "-fx-border-color: transparent; " +
                                "-fx-border-width: 2px;" +
                                "-fx-border-style: solid;" +
                                "-fx-border-radius: 2 2 2 2;"
                );
            } else {
                outputPathField.setStyle(
                        "-fx-border-color: red; " +
                                "-fx-border-width: 2px;" +
                                "-fx-border-style: solid;" +
                                "-fx-border-radius: 2 2 2 2;"
                );
            }
        });

        sourceFilePath.textProperty().setValue("");
        outputPathField.textProperty().setValue("");

    }

    private boolean isSourcePathValid(String path) {
        File file = new File(path);
        return file.exists() && !file.isDirectory();
    }

    private boolean isOutputPathValid(String path) {
        File file = new File(path);
        return !file.isDirectory();
    }

    public void setStage(Stage stage) {
        this.windowStage = stage;
    }

    public void onCloseClicked(ActionEvent actionEvent) {
        windowStage.close();
    }

    public void onCompileClicked(ActionEvent actionEvent) {
        if(!isSourcePathValid(sourceFilePath.getText())) {
            outputArea.appendText("Please input a valid source file path!\n");
            return;
        }

        if(!isOutputPathValid(outputPathField.getText())) {
            outputArea.appendText("Please input a valid output file path!\n");
            return;
        }

        PrintStream logStream = new PrintStream(new CompilerStream(outputArea));

        CompileUnit compileUnit = new CompileUnit(
                new File(sourceFilePath.getText()),
                new File(outputPathField.getText()),
                "" + System.nanoTime(),
                logStream
        );

        Task compileTask = new CompileTask(compileUnit, logStream);
        compilerProgress.progressProperty().bind(compileTask.progressProperty());
        new Thread(compileTask).start();
    }

    public void onSourceBrowse(ActionEvent actionEvent) {
        File sourceFile = new FileDialogBuilder(FileDialogBuilder.FileDialogType.OPEN)
                .setOwner(windowStage)
                .setTitle("Select runimation source file...")
                .addFilter(FileDialogBuilder.FileFilters.ALL_FILES)
                .addFilter(FileDialogBuilder.FileFilters.JAVA_FILES)
                .show();

        if(sourceFile != null && sourceFile.exists() && !sourceFile.isDirectory())
            sourceFilePath.setText(sourceFile.getAbsolutePath());
    }

    public void onDestinationBrowse(ActionEvent actionEvent) {
        File zipFile = new FileDialogBuilder(FileDialogBuilder.FileDialogType.SAVE)
                .setOwner(windowStage)
                .setTitle("Select output zip file...")
                .addFilter(FileDialogBuilder.FileFilters.ZIP_FILES)
                .show();

        if(zipFile != null && !zipFile.isDirectory())
            outputPathField.setText(zipFile.getAbsolutePath());
    }
}
