package at.runimation.vis;

import at.runimation.log.designator.ConstDesignator;
import at.runimation.log.designator.ElemDesignator;
import at.runimation.log.designator.IDesignatorProcessor;
import at.runimation.log.designator.VarDesignator;
import at.runimation.vis.transaction.ConstTransaction;
import at.runimation.vis.transaction.ElemTransaction;
import at.runimation.vis.transaction.ITransaction;
import at.runimation.vis.transaction.VarTransaction;

import java.util.Map;

public class ModelProcessor implements IDesignatorProcessor<ITransaction> {
    public static final ModelProcessor INSTANCE = new ModelProcessor();

    private ModelProcessor() {}

    @Override
    public ITransaction process(ConstDesignator des, Map<String, Object> params) {
        // Nothing to assignFrom
        return new ConstTransaction(des.getValue());
    }

    @Override
    public ITransaction process(VarDesignator des, Map<String, Object> params) {
        //Model.getInstance().setVarValue(des.getName(), value);
        return new VarTransaction(des.getName());
    }

    @Override
    public ITransaction process(ElemDesignator des, Map<String, Object> params) {
        //Model.getInstance().setArrValue(des.getArray(), des.getIndex(), value);
        return new ElemTransaction(des.getArray(), des.getIndex());
    }
}
