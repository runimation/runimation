int[] a = new int[] { 21, 75, 72, 88, 61, 85, 100, 36, 13, 19 };    //$ def int[] a ; init a
int n = a.length;

int h;                                      //$ def int h
int shifts = 0;                             //$ def int shifts ; shifts = 0
int i = 0;                                  //$ def int i ; i = 0 ; idx a, i

while(i < n-1) {                            //$ comp i < n-1
    h = a[i + 1];                           //$ h = a[i + 1]
    int j = i;                              //$ def int j ; j = i ; idx a, j

    while (j >= 0 &&                        //$ comp j >= 0
           a[j] > h) {                      //$ comp a[j] > h
        a[j + 1] = a[j];                    //$ a[j + 1] = a[j]
        shifts++;                           //$ inc shifts
        j--;                                //$ inc j
    }

    a[j + 1] = h;                           //$ a[j + 1] = h
    i++;                                    //$ inc i
}