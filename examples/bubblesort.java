                                        //$ animate off
int[] a = {55, 42, 8, 12, 15, 98, 77, 86, 22, 23, 25, 20, 17, 18}; //$ def int[] a ; init a
int temp;
int i = 0;                              //$ def int i ; i = 0
                                        //$ animate on

while(i < a.length) {                   //$ comp i < a.length
    int j = 0;                          //$ def int j ; j = 0 ; idx a, j
    while(j < a.length - i - 1) {       //$ comp j < a.length - i - 1
        if(a[j] > a[j+1])               //$ comp a[j] > a[j+1]
        {
            temp = a[j];                //$ swap a[j], a[j+1]
            a[j] = a[j+1];
            a[j+1] = temp;
        }
        j++;                            //$ inc j
    }
    i++;                                //$ inc i
}