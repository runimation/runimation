/*
 * Copyright 2018 Stefan Grünzinger, Florian Schachermair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.runimation.log.designator;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;

public class VarDesignator implements IDesignator {
    private String name;

    VarDesignator() {

    }

    public VarDesignator(String name) {
        Objects.requireNonNull(name);
        this.name = name;
    }

    @Override
    public DesignatorType getType() {
        return DesignatorType.VARIABLE;
    }

    @Override
    public void read(Object obj) {
        Objects.requireNonNull(obj);

        if(!(obj instanceof String)) {
            System.out.println("Invalid variable designator: expected String, got [" + obj.getClass().getSimpleName() + "]");
            return;
        }

        this.name = (String)obj;
    }

    @Override
    public Object write() {
        return name;
    }

    @Override
    public void dump() {
        System.out.println("VAR " + name);
    }

    @Override
    public <T> T process(IDesignatorProcessor<T> processor, Map<String, Object> params) {
        return processor.process(this, params);
    }

    public String getName() {
        return name;
    }
}
