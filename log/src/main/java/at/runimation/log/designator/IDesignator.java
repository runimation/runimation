/*
 * Copyright 2018 Stefan Grünzinger, Florian Schachermair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.runimation.log.designator;

import org.json.JSONObject;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;

public interface IDesignator {
    /**
     * Get the type of the designator
     *
     * @return The type of the designator
     */
    DesignatorType getType();

    /**
     * Loads the values of the designator from the given object
     *
     * @param obj The object to read the values from. This Object gets instantiated by the JSON library.
     */
    void read(Object obj);

    /**
     * Saves the stored values to an Object, which will be saved as a value in the JSON
     *
     * @return The Object containing the information
     */
    Object write();

    /**
     * Dumps all the designator content to System.out
     */
    void dump();

    /**
     * Processes the designator with the specified processor
     * @param processor The processor to process the designator with
     * @param params
     */
    <T> T process(IDesignatorProcessor<T> processor, Map<String, Object> params);

    default <T> T process(IDesignatorProcessor<T> processor) {
        return process(processor, Collections.emptyMap());
    }

    /**
     * Parse the designator from the JSON file and return a complete IDesignator instance.
     *
     * @param obj The JSON object to load the designator from
     * @return The complete and loaded instance
     */
    static IDesignator parseDesignator(JSONObject obj) {
        for(DesignatorType type : DesignatorType.values()) {
            if(obj.has(type.getKey().toLowerCase())) { // Found type
                IDesignator designator = null;

                // Instantiate designator
                switch(type) {
                    case CONST:
                        designator = new ConstDesignator();
                        break;
                    case VARIABLE:
                        designator = new VarDesignator();
                        break;
                    case ELEMENT:
                        designator = new ElemDesignator();
                        break;
                }

                // Load value into designator
                designator.read(obj.get(type.getKey()));
                return designator;
            }
        }

        System.out.println(">>> Invalid designator:\n" + obj.toString(1));
        return null;
    }

    /**
     * Write the designator to a JSON object.
     *
     * @param designator The designator to write to the JSON object
     * @param obj The object to
     * @return
     */
    static JSONObject writeDesignator(IDesignator designator, JSONObject obj) {
        Objects.requireNonNull(designator, "Designator must not be null!");

        // Get the key from the type and the value from the designator
        obj.put(designator.getType().getKey(), designator.write());

        return obj;
    }
}
