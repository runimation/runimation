package at.runimation.log.designator;

import java.util.Map;

public interface IDesignatorProcessor<T> {
    T process(ConstDesignator des, Map<String, Object> params);
    T process(VarDesignator des, Map<String, Object> params);
    T process(ElemDesignator des, Map<String, Object> params);
}
