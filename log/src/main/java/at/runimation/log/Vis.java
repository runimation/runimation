/*
 * Copyright 2018 Stefan Grünzinger, Florian Schachermair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.runimation.log;

import at.runimation.log.action.*;
import at.runimation.log.designator.IDesignator;
import at.runimation.log.variable.ArrayVariable;
import at.runimation.log.variable.Index;
import at.runimation.log.variable.SimpleVariable;

public class Vis {
    public static void defineVar(String name, String type) {
        LogOut.addVariable(new SimpleVariable(name, type));
    }

    public static void defineArray(String name, String type, int length) {
        LogOut.addVariable(new ArrayVariable(name, type, length));
    }

    public static void defineIndex(String arr, String var) {
        LogOut.addIndex(new Index(var, arr));
    }

    public static void initArray(int line, String name, Object elements) {
        LogOut.addAction(new InitArrAction(line, name, elements));
    }

    public static void animate(int line, boolean shouldAnimate) {
        LogOut.addAction(new AnimateAction(line, shouldAnimate));
    }

    public static void assign(int line, IDesignator left, IDesignator right) {
        LogOut.addAction(new AssignAction(line, left, right));
    }

    public static boolean compare(int line, boolean result, IDesignator left, IDesignator right, String type) {
        LogOut.addAction(new CompareAction(line, left, right, type, result));
        return result;
    }

    public static void swap(int line, IDesignator left, IDesignator right) {
        LogOut.addAction(new SwapAction(line, left, right));
    }

    public static void increment(int line, IDesignator left, Object result) {
        LogOut.addAction(new IncrementAction(line, left, result));
    }

    public static void decrement(int line, IDesignator left, Object result) {
        LogOut.addAction(new DecrementAction(line, left, result));
    }
}
