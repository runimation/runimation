/*
 * Copyright 2018 Stefan Grünzinger, Florian Schachermair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.runimation.log;

import at.runimation.log.action.IAction;
import at.runimation.log.variable.IVariable;
import at.runimation.log.variable.Index;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class LogIn {
    private static JSONArray variables;
    private static JSONArray indices;
    private static JSONArray actions;

    private static String source;

    /**
     * Get the number of Variables in the log file
     *
     * @return The number of variables
     */
    public static int getNumberVariables() {
        return variables.length();
    }

    /**
     * Get the number of Actions in the log file
     *
     * @return The number of actions
     */
    public static int getNumberActions() {
        return actions.length();
    }

    /**
     * Get the number of Indices in the log file
     *
     * @return The number of indices
     */
    public static int getNumberIndices() {
        return indices.length();
    }

    /**
     * Get a variable from the log file by the index
     *
     * @param idx The index of the variable to get, must be in the bounds of 0 to getNumberVariables()
     * @return The variable by index
     */
    public static IVariable getVariable(int idx) {
        return IVariable.parseVariable(variables.getJSONObject(idx));
    }

    /**
     * Get an action from the log file by the index
     *
     * @param idx The index of the action to get, must be in the bounds of 0 to getNumberActions()
     * @return The variable by index
     */
    public static IAction getAction(int idx) {
        return IAction.parseAction(actions.getJSONObject(idx));
    }

    /**
     * Get an index from the log file by the index
     *
     * @param idx The index of the index to get, must be in the bounds of 0 to getNumberIndex()
     * @return The variable by index
     */
    public static Index getIndex(int idx) {
        return Index.parseIndex(indices.getJSONObject(idx));
    }

    public static boolean loadFromZip(File zipFilePath) {
        try {
            ZipFile zipFile = new ZipFile(zipFilePath);

            boolean foundMain = false;
            boolean foundLog = false;

            Enumeration<? extends ZipEntry> entries = zipFile.entries();
            while(entries.hasMoreElements()) {
                ZipEntry entry = entries.nextElement();

                System.out.println(entry.getName());

                if(entry.getName().equals("Main.java")) {
                    foundMain = true;
                    source = readSource(zipFile.getInputStream(entry));
                }

                if(entry.getName().equals("log.json")) {
                    foundLog = true;
                    readLog(zipFile.getInputStream(entry));
                }
            }

            zipFile.close();

            if(!foundMain) {
                System.err.println("Could not find source file in zip!");
                return false;
            }

            if(!foundLog) {
                System.err.println("Could not find log file in zip!");
                return false;
            }

            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    /**
     * Get the source code without instrumentation instructions
     *
     * @return The sourcecode without instrumentation
     */
    public static String getSource() {
        return source;
    }

    /**
     * Dump all loaded data to System.out
     */
    public static void dumpAll() {
        System.out.println("Variables: ");
        for(int i = 0; i < getNumberVariables(); i++) {
            IVariable var = getVariable(i);

            if(var == null)
                continue;

            var.dump();
        }
        System.out.println();

        System.out.println("Indices: ");
        for(int i = 0; i < getNumberIndices(); i++) {
            Index idx = getIndex(i);

            System.out.println(String.format(" - %s <- %s", idx.getArray(), idx.getVariable()));
        }

        System.out.println("Actions");
        for(int i = 0; i < getNumberActions(); i++) {
            IAction action = getAction(i);

            System.out.printf(" - %s\n", action.getType().name());

            action.dump();
        }
    }

    /**
     * Read the source file and remove the instrumentation instructions
     *
     *
     * @return The sourcecode without instrumentation instructions
     * @throws IOException
     */
    private static String readSource(InputStream inputStream) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder sourceBuilder = new StringBuilder();

        int instIndex;

        String line;
        while((line = reader.readLine()) != null) {
            instIndex = line.indexOf("//$");

            if(instIndex != -1) {
                String realSourceLine = line.substring(0, instIndex);
                if(!realSourceLine.trim().isEmpty())
                    sourceBuilder.append(realSourceLine).append('\n');
            } else {
                sourceBuilder.append(line).append('\n');
            }
        }

        reader.close();

        return sourceBuilder.toString();
    }

    /**
     * Read the log file and load the variables, actions and indices
     *
     * @throws IOException
     */
    private static void readLog(InputStream inputStream) throws IOException {
        StringBuilder jsonStringBuilder = new StringBuilder();

        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        String line;
        while((line = reader.readLine()) != null) {
            jsonStringBuilder.append(line);
        }

        JSONObject rootObj = new JSONObject(jsonStringBuilder.toString());

        variables = rootObj.getJSONArray("variables");
        actions = rootObj.getJSONArray("actions");
        indices = rootObj.getJSONArray("indices");
    }
}