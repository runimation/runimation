/*
 * Copyright 2018 Stefan Grünzinger, Florian Schachermair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.runimation.log;

import at.runimation.log.action.IAction;
import at.runimation.log.variable.IVariable;
import at.runimation.log.variable.Index;
import at.runimation.utils.IOHelper;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class LogOut {
    private static ArrayList<IVariable> variables = new ArrayList<>();
    private static ArrayList<Index> indices = new ArrayList<>();
    private static ArrayList<IAction> actions = new ArrayList<>();

    static void addVariable(IVariable variable) {
        if(variables.contains(variable))
            return;

        variables.add(variable);
    }

    static void addIndex(Index index) {
        if(indices.contains(index))
            return;

        indices.add(index);
    }

    static void addAction(IAction action) {
        actions.add(action);
    }

    public static void writeLog(File file) {
        JSONObject rootObj = new JSONObject();

        JSONArray variableArray = new JSONArray();
        JSONArray indexArray = new JSONArray();
        JSONArray actionArray = new JSONArray();

        for (IVariable var : variables) {
            variableArray.put(IVariable.writeVariable(var, new JSONObject()));
        }

        for (Index idx : indices) {
            indexArray.put(idx.write(new JSONObject()));
        }

        for (IAction action : actions) {
            actionArray.put(IAction.writeAction(action, new JSONObject()));
        }

        rootObj.put("variables", variableArray);
        rootObj.put("indices", indexArray);
        rootObj.put("actions", actionArray);

        try {
            IOHelper.writeFromString(file, rootObj.toString(1));
        } catch (IOException e) {
            e.printStackTrace();
        }

        reset();
    }

    public static void reset() {
        variables.clear();
        indices.clear();
        actions.clear();
    }

}
