/*
 * Copyright 2018 Stefan Grünzinger, Florian Schachermair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.runimation.log.action;

import org.json.JSONObject;

import java.util.Objects;

public interface IAction {
    /**
     * Get the action type
     *
     * @return The action type
     */
    ActionType getType();

    /**
     * Read the saved action parameters from a JSON object
     *
     * @param obj The JSON object to read the data from
     */
    void read(JSONObject obj);

    /**
     * Save the action parameters to the JSON object
     *
     * This method has to return the given JSON object
     *
     * @param obj The object to save the data to
     * @return The given JSON object in the parameters
     */
    JSONObject write(JSONObject obj);

    /**
     * Dump the action parameters to System.out
     */
    void dump();

    /**
     * Parse the action type from the JSON object and return a new Action object with the loaded parameters
     *
     * @param obj The JSON object to load the data from
     * @return The loaded action object
     */
    static IAction parseAction(JSONObject obj) {
        String typeString = obj.getString("action");
        ActionType type = ActionType.valueOf(typeString);

        IAction action = null;

        switch (type) {
            case ANIMATE:
                action = new AnimateAction();
                break;
            case ASSIGN:
                action = new AssignAction();
                break;
            case INIT_ARR:
                action = new InitArrAction();
                break;
            case COMPARE:
                action = new CompareAction();
                break;
            case SWAP:
                action = new SwapAction();
                break;
            case INCREMENT:
                action = new IncrementAction();
                break;
            case DECREMENT:
                action = new DecrementAction();
                break;
        }

        action.read(obj);

        return action;
    }

    static JSONObject writeAction(IAction action, JSONObject obj) {
        Objects.requireNonNull(action, "Action must not be null!");

        obj.put("action", action.getType());

        action.write(obj);

        return obj;
    }
}
