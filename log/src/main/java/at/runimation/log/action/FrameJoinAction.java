/*
 * Copyright 2018 Stefan Grünzinger, Florian Schachermair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.runimation.log.action;

import org.json.JSONObject;

public class FrameJoinAction extends LinedAction {
    private String frameName;

    public FrameJoinAction() {
        this("");
    }

    public FrameJoinAction(String frameName) {
        this.frameName = frameName;
    }

    @Override
    public JSONObject write(JSONObject obj) {
        obj.put("frameName", frameName);

        return super.write(obj);
    }

    @Override
    public void read(JSONObject obj) {
        this.frameName = obj.getString("frameName");

        super.read(obj);
    }

    @Override
    public ActionType getType() {
        return ActionType.FRAME_JOIN;
    }
}
