/*
 * Copyright 2018 Stefan Grünzinger, Florian Schachermair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.runimation.log.action;

import at.runimation.log.designator.IDesignator;
import org.json.JSONObject;

public class CompareAction extends BinaryAction {
    private String compareType;
    private boolean result;

    CompareAction() {
    }

    public CompareAction(int line, IDesignator lhs, IDesignator rhs, String compareType, boolean result) {
        super(line, lhs, rhs);
        this.compareType = compareType;
        this.result = result;
    }

    @Override
    public ActionType getType() {
        return ActionType.COMPARE;
    }

    @Override
    public void read(JSONObject obj) {
        super.read(obj);

        compareType = obj.getString("type");
        result = obj.getBoolean("res");
    }

    @Override
    public JSONObject write(JSONObject obj) {
        super.write(obj);

        obj.put("type", compareType);
        obj.put("res", result);

        return obj;
    }

    @Override
    public void dump() {
        super.dump();
    }

    public String getCompareType() {
        return compareType;
    }

    public boolean getResult() {
        return result;
    }
}
