/*
 * Copyright 2018 Stefan Grünzinger, Florian Schachermair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.runimation.log.action;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Arrays;

public class InitArrAction extends LinedAction {
    private String name;
    private Object[] elements;

    InitArrAction() {
    }

    public InitArrAction(int line, String name, Object elements) {
        super(line);
        this.name = name;
        this.elements = new JSONArray(elements).toList().toArray();
    }

    @Override
    public ActionType getType() {
        return ActionType.INIT_ARR;
    }

    @Override
    public void read(JSONObject obj) {
        super.read(obj);

        name = obj.getString("name");
        elements = obj.getJSONArray("elem").toList().toArray();
    }

    @Override
    public JSONObject write(JSONObject obj) {
        obj.put("name", name);
        obj.put("elem", elements);

        return super.write(obj);
    }

    @Override
    public void dump() {
        super.dump();
        System.out.println("   - NAME " + name);
        System.out.println("   - ELEM " + Arrays.toString(elements));
    }

    public String getName() {
        return name;
    }

    public Object[] getElements() {
        return elements;
    }
}
