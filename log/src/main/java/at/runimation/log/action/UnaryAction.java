/*
 * Copyright 2018 Stefan Grünzinger, Florian Schachermair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.runimation.log.action;

import at.runimation.log.designator.IDesignator;
import org.json.JSONObject;

public abstract class UnaryAction extends LinedAction {
    private IDesignator designator;

    UnaryAction() {
    }

    public UnaryAction(int line, IDesignator designator) {
        super(line);
        this.designator = designator;
    }

    @Override
    public void read(JSONObject obj) {
        super.read(obj);

        designator = IDesignator.parseDesignator(obj);
    }

    @Override
    public JSONObject write(JSONObject obj) {
        super.write(obj);

        // Write designator directly into JSONObject
        IDesignator.writeDesignator(designator, obj);

        return obj;
    }

    public IDesignator getDesignator() {
        return designator;
    }

    @Override
    public void dump() {
        super.dump();
        System.out.print("   - ");
        designator.dump();
    }
}
