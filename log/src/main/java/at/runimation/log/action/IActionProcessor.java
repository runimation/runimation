package at.runimation.log.action;

public interface IActionProcessor {
    void process(AnimateAction action);
    void process(AssignAction action);
    void process(CompareAction action);
    void process(DecrementAction action);
    void process(FrameExitAction action);
    void process(FrameJoinAction action);
    void process(IncrementAction action);
    void process(InitArrAction action);
    void process(SwapAction action);
}
