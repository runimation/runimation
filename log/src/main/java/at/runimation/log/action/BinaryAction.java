/*
 * Copyright 2018 Stefan Grünzinger, Florian Schachermair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.runimation.log.action;

import at.runimation.log.designator.IDesignator;
import org.json.JSONObject;

public abstract class BinaryAction extends LinedAction {
    private IDesignator lhs;
    private IDesignator rhs;

    BinaryAction() {

    }

    public BinaryAction(int line, IDesignator lhs, IDesignator rhs) {
        super(line);
        this.lhs = lhs;
        this.rhs = rhs;
    }

    @Override
    public void read(JSONObject obj) {
        super.read(obj);

        lhs = IDesignator.parseDesignator(obj.getJSONObject("lhs"));
        rhs = IDesignator.parseDesignator(obj.getJSONObject("rhs"));
    }

    @Override
    public JSONObject write(JSONObject obj) {
        super.write(obj);

        obj.put("lhs", IDesignator.writeDesignator(lhs, new JSONObject()));
        obj.put("rhs", IDesignator.writeDesignator(rhs, new JSONObject()));

        return obj;
    }

    @Override
    public void dump() {
        super.dump();
        System.out.print("   - LEFT ");
        lhs.dump();

        System.out.print("   - RIGHT ");
        rhs.dump();
    }

    public IDesignator getLeftDesignator() {
        return lhs;
    }

    public IDesignator getRightDesignator() {
        return rhs;
    }
}
