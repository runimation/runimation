/*
 * Copyright 2018 Stefan Grünzinger, Florian Schachermair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.runimation.log.action;

import at.runimation.log.designator.IDesignator;
import org.json.JSONObject;

public class IncrementAction extends UnaryAction {
    private Object result;

    IncrementAction() {
    }

    public IncrementAction(int line, IDesignator designator, Object result) {
        super(line, designator);
        this.result = result;
    }

    @Override
    public ActionType getType() {
        return ActionType.INCREMENT;
    }

    @Override
    public void read(JSONObject obj) {
        super.read(obj);

        result = obj.get("res");
    }

    @Override
    public JSONObject write(JSONObject obj) {
        super.write(obj);

        obj.put("res", result);

        return obj;
    }

    @Override
    public void dump() {
        super.dump();
        System.out.println("   - RESULT " + result);
    }

    public Object getResult() {
        return result;
    }
}
