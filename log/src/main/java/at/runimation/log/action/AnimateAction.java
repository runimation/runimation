/*
 * Copyright 2018 Stefan Grünzinger, Florian Schachermair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.runimation.log.action;

import org.json.JSONObject;

public class AnimateAction extends LinedAction {
    private boolean enable;

    AnimateAction() {
    }

    public AnimateAction(int line, boolean enable) {
        super(line);
        this.enable = enable;
    }

    @Override
    public ActionType getType() {
        return ActionType.ANIMATE;
    }

    @Override
    public void read(JSONObject obj) {
        super.read(obj);

        enable = obj.getBoolean("ena");
    }

    @Override
    public JSONObject write(JSONObject obj) {
        super.write(obj);

        obj.put("ena", enable);

        return obj;
    }

    @Override
    public void dump() {
        super.dump();
        System.out.println("   - ENABLE " + enable);
    }

    public boolean getEnable() {
        return enable;
    }
}
