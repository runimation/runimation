/*
 * Copyright 2018 Stefan Grünzinger, Florian Schachermair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.runimation.log.variable;

import org.json.JSONObject;

public class Index {
    private String var;
    private String array;

    private Index() {

    }

    public Index(String var, String array) {
        this.var = var;
        this.array = array;
    }

    public void read(JSONObject obj) {
        var = obj.getString("var");
        array = obj.getString("arr");
    }

    public JSONObject write(JSONObject obj) {
        obj.put("arr", array);
        obj.put("var", var);

        return obj;
    }

    public String getVariable() {
        return var;
    }

    public String getArray() {
        return array;
    }

    public static Index parseIndex(JSONObject obj) {
        Index idx = new Index();

        idx.read(obj);

        return idx;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Index index = (Index) o;

        if (var != null ? !var.equals(index.var) : index.var != null) return false;
        return array != null ? array.equals(index.array) : index.array == null;
    }
}
