/*
 * Copyright 2018 Stefan Grünzinger, Florian Schachermair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.runimation.log.variable;

import org.json.JSONObject;

public interface IVariable {
    VariableType getKind();
    String getName();

    void read(JSONObject obj);
    JSONObject write(JSONObject obj);
    void dump();

    static IVariable parseVariable(JSONObject obj) {
        String typeString = obj.getString("kind");
        VariableType type = VariableType.valueOf(typeString);

        IVariable var = null;

        switch(type) {
            case SIMPLE:
                var = new SimpleVariable();
                break;
            case ARRAY:
                var = new ArrayVariable();
                break;
        }

        var.read(obj);

        return var;
    }

    static JSONObject writeVariable(IVariable variable, JSONObject obj) {
        obj.put("kind", variable.getKind());

        variable.write(obj);

        return obj;
    }
}
