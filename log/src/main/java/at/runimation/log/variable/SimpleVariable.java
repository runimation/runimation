/*
 * Copyright 2018 Stefan Grünzinger, Florian Schachermair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.runimation.log.variable;

import org.json.JSONObject;

public class SimpleVariable implements IVariable {
    private String name;
    private String type;

    SimpleVariable() {
    }

    public SimpleVariable(String name, String type) {
        this.name = name;
        this.type = type;
    }


    @Override
    public VariableType getKind() {
        return VariableType.SIMPLE;
    }

    @Override
    public void read(JSONObject obj) {
        name = obj.getString("name");
        type = obj.getString("type");
    }

    @Override
    public JSONObject write(JSONObject obj) {
        obj.put("name", name);
        obj.put("type", type);

        return obj;
    }

    @Override
    public void dump() {
        System.out.printf("   - %s %s\n", getType(), getName());
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SimpleVariable that = (SimpleVariable) o;

        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        return type != null ? type.equals(that.type) : that.type == null;
    }
}
